package 
{
	import flash.display.MovieClip;
	/**
	 * Holds properties that are required by each player in "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruPlayer extends MovieClip
	{
		//PROPERTIES / Variables
		private var _canReroll:Boolean;				//determines whether or not the player can play the Reroll card (false by default, true when Roll is clicked, set to false when a card is drawn, a piece is placed, or turn is ended)
		private var _extraTurnCount:int;			//number of extra turns that this player can use
		private var _numPieces:int;					//number of pieces on the board owned by this player
		private var _numShields:int;				//number of shield cards the player is holding
		private var _pieceFrame:int;				//the frame number of the icon that corresponds to this player's game piece
		private var _skipTurnCount:int;				//number of turns that this player has to skip before it can become their turn again
		
		//PROPERTIES / Objects
		private var _opponent:PatruPlayer;
		
		//PROPERTIES / Lists
		private var _cardSlots:Array = [];			//list of the slot objects that the player's cards fit into
		private var _hand:Array = [];				//list of cards in this player's hand
		
		
		//GETTER/SETTERS
		public function get canReroll():Boolean 
		{
			return _canReroll;
		}
		
		public function set canReroll(value:Boolean):void 
		{
			_canReroll = value;
		}
		
		public function get cardSlots():Array 
		{
			return _cardSlots;
		}
		
		public function set cardSlots(value:Array):void 
		{
			_cardSlots = value;
		}
		
		public function get extraTurnCount():int 
		{
			return _extraTurnCount;
		}
		
		public function set extraTurnCount(value:int):void 
		{
			_extraTurnCount = value;
		}
		
		public function get hand():Array 
		{
			return _hand;
		}
		
		public function set hand(value:Array):void 
		{
			_hand = value;
		}
		
		public function get numPieces():int 
		{
			return _numPieces;
		}
		
		public function set numPieces(value:int):void 
		{
			_numPieces = value;
		}
		
		public function get numShields():int 
		{
			return _numShields;
		}
		
		public function set numShields(value:int):void 
		{
			_numShields = value;
		}

		public function get opponent():PatruPlayer 
		{
			return _opponent;
		}
		
		public function set opponent(value:PatruPlayer):void 
		{
			_opponent = value;
		}
		
		public function get pieceFrame():int 
		{
			return _pieceFrame;
		}
		
		public function set pieceFrame(value:int):void 
		{
			_pieceFrame = value;
		}
		
		public function get skipTurnCount():int 
		{
			return _skipTurnCount;
		}
		
		public function set skipTurnCount(value:int):void 
		{
			_skipTurnCount = value;
		}
	}
}