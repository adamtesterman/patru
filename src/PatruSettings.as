package 
{
	/**
	 * Contains static constants that can be accessed throughout the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruSettings
	{
		public static const RIGHT_EDGE:int =				640;
		public static const BOTTOM_EDGE:int =				480;
		
		public static const AI_ANIMATION_LENGTH:int =		15;
		
		public static const THEME_BASIC:String =			"basic";
		public static const THEME_BEACH:String =			"beach";
		public static const THEME_LAVA:String =				"lava";
		public static const THEME_LAWN:String =				"lawn";
		public static const THEME_MUSHROOM:String =			"mushroom";
		public static const THEME_RANDOM:String =			"rand";			//"random" is a keyword used by Flash and cannot be used here
		public static const THEME_RETRO:String =			"retro";
		public static const THEME_SNOW:String =				"snow";
		public static const THEME_SPACE:String =			"space";
		
		public static const THEMES:Array = [				THEME_BASIC,	//list of themes, so that when random is selected, one can be chosen from this list
															THEME_BASIC,
															THEME_LAVA,
															THEME_LAWN,
															THEME_MUSHROOM,
															THEME_RETRO,
															THEME_SNOW,
															THEME_SPACE
																			];
	}
}