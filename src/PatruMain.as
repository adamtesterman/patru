package 
{
	import flash.display.*;
	import flash.events.*;
	
	import events.GameEvent;
	import ui.screens.*;
	import utils.SoundEngine;
	
	/**
	 * Document class for the game "Patru."
	 * 
	 * "Patru" was designed by Matthew Russell. The AI system for "Patru" was designed by Michael R. Stokes, Jr.
	 * 
	 * @author Adam Testerman
	 */
	public class PatruMain extends GameBase
	{
		//PROPERTIES / Constants
		private const CARD_ADD_PIECE:String =								"addpiece";					//card labels
		private const CARD_BATTLE:String =									"battle";
		private const CARD_DISCARD_HAND:String =							"discardhand";
		private const CARD_EXTRA_TURN:String =								"extraturn";
		private const CARD_GIVE_CARD:String =								"givecard";
		private const CARD_LOSE_NEXT_TURN:String =							"losenextturn";
		private const CARD_LOSE_RANDOM_PIECE:String =						"loserandompiece";
		private const CARD_REMOVE_PIECE:String =							"removepiece";
		private const CARD_REROLL:String =									"reroll";
		private const CARD_REROLL_OPPONENT:String =							"rerollopponent";
		private const CARD_SHIELD:String =									"shield";
		private const CARD_STEAL_CARD:String =								"stealcard";
		
		private const GAME_ADDING_PIECE:int =								3;							//game states specific to this game
		private const GAME_BATTLING:int =									4;
		private const GAME_REMOVING_PIECE:int =								5;
		
		//PROPERTIES / Constants (Audio Labels)
		private const SFX_DICE_ROLL_01:String =								"DiceRoll01";
		
		//PROPERTIES / Variables
		private var _currentTheme:int;
		private var _roll1:int;												//dice rolls for this turn (d6)
		private var _roll2:int;
		private var _placedNormalPieceThisTurn:Boolean;						//whether or not a dice roll piece was placed (not a piece from Add Piece or Battle cards)
		
		//PROPERTIES / Lists
		private var _cardsPositive:Array = 	[	CARD_REROLL, 				//list of all cards that benefit the player that uses them
												CARD_REMOVE_PIECE, 
												CARD_ADD_PIECE, 
												CARD_STEAL_CARD, 
												CARD_EXTRA_TURN, 
												CARD_SHIELD, 
												CARD_BATTLE ];
		
		private var _cardsNegative:Array = [	CARD_GIVE_CARD, 			//list of all cards that are detrimental to the player that uses them
												CARD_LOSE_NEXT_TURN, 
												CARD_DISCARD_HAND, 
												CARD_LOSE_RANDOM_PIECE ];
		
		private var _cardDeckMaster:Array = [	CARD_REROLL, 				//list of all cards in the actual deck that is used in the game
												CARD_REMOVE_PIECE, 
												CARD_REMOVE_PIECE, 
												CARD_ADD_PIECE, 
												CARD_ADD_PIECE,
												CARD_STEAL_CARD, 
												CARD_EXTRA_TURN, 
												CARD_EXTRA_TURN, 
												CARD_SHIELD, 
												CARD_GIVE_CARD, 
												CARD_LOSE_NEXT_TURN, 
												CARD_DISCARD_HAND, 
												CARD_LOSE_RANDOM_PIECE, 
												CARD_BATTLE ];
		private var _cardDeck:Array = [];									//copies the master deck in order to allow for removal of cards, resets to master deck each new game
		
		//PROPERTIES / Objects
		private var _se:SoundEngine = SoundEngine.getInstance();
		private var _p1:PatruPlayer;
		private var _p2:PatruPlayer;
		private var _turnPlayer:PatruPlayer;
		private var _nonTurnPlayer:PatruPlayer;
		private var _battleWinner:PatruPlayer;								//the winner of the Battle card that can be played
		private var _firstSquare:PatruBoardSquare;									//active squares for this turn, based on dice rolls
		private var _secondSquare:PatruBoardSquare;
		
		//PROPERTIES / Game Screens
		private var _screenAIStatus:PatruScreenAIStatus;
		private var _screenCardDialog:PatruScreenCardDialog;
		private var _screenCredits:PatruScreenCredits;
		private var _screenEndTurnDialog:PatruScreenEndTurnDialog;
		private var _screenGameOver:PatruScreenGameOver;
		private var _screenHelp:PatruScreenHelp;
		private var _screenMainMenu:PatruScreenMainMenu;
		private var _screenRollBattle:PatruScreenRollBattle;
		private var _screenShielded:PatruScreenShielded;
		private var _screenThemeSelect:PatruScreenThemeSelect;
		
		
		/**
		 * Constructor for the game "Patru."
		 * 
		 * Relies on the <code>GameBase</code> constructor to continue.
		 */
		public function PatruMain():void 
		{
			
		}
		
		
		/**
		 * (Override Method)
		 * 
		 * Initializes all game content and opens the main menu.
		 * 
		 * @param	event	The optional <code>ADDED_TO_STAGE</code> event object.
		 */
		protected override function initGame(event:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initGame);
			
			//New methods
			SetGameState(GameBase.GAME_OVER);
			AddOverallEventListeners();
			OpenMainMenu();
		}
		
		
		/**
		 * Starts/restarts the gameplay.
		 */
		private function StartGame(event:Event = null):void 
		{
			//New properties
			_p1.numPieces = 0;
			_p2.numPieces = 0;
			_p1.hand = [];
			_p2.hand = [];
			for (var i:int = 0; i < _cardDeckMaster.length; i++)	//reset ("shuffle") card deck
			{
				_cardDeck[i] = _cardDeckMaster[i];
			}
			
			//New methods
			CloseAllScreens();
			DisableBoard(true);
			SetGameState(GameBase.GAME_PLAYING);
			SwitchToPlayer(_p1, false, false);
			
			//Inherited methods
			bg.gotoAndStop(turnPlayerIndicator.currentLabel);
			die1.gotoAndStop(1);
			die2.gotoAndStop(1);
		}
		
		
		/**
		 * Adds all event listeners that are used regardless of the game state.
		 */
		private function AddOverallEventListeners():void 
		{
			addEventListener(GameEvent.CLOSE_SCREEN, CloseScreen);
		}
		
		
		/**
		 * Removes all event listeners that are used regardless of the game state.
		 */
		private function RemoveOverallEventListeners():void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, initGame);
			removeEventListener(GameEvent.CLOSE_SCREEN, CloseScreen);
		}
		
		
		/**
		 * Adds all event listeners that are only relevant during gameplay.
		 */
		private function AddInGameEventListeners():void 
		{
			btnQuit.addEventListener(MouseEvent.CLICK, QuitToMain);
			btnRoll.addEventListener(MouseEvent.CLICK, RollDice);
			btnDraw.addEventListener(MouseEvent.CLICK, DrawCard);
			btnEndTurn.addEventListener(MouseEvent.CLICK, EndTurn);
		}
		
		
		/**
		 * Removes all event listeners that are only relevant during gameplay.
		 */
		private function RemoveInGameEventListeners():void 
		{
			btnQuit.removeEventListener(MouseEvent.CLICK, QuitToMain);
			btnRoll.removeEventListener(MouseEvent.CLICK, RollDice);
			btnDraw.removeEventListener(MouseEvent.CLICK, DrawCard);
			btnEndTurn.removeEventListener(MouseEvent.CLICK, EndTurn);
		}
		
		
		/**
		 * Determines whether or not a certain card can be played from a certain player's perspective. Checks for card-specific requirements in order to determine such.
		 * 
		 * @param	player		The player who would be playing the card.
		 * @param	card		The card that would be played.
		 * 
		 * @return				Whether or not the card can be played by the passed player.
		 */
		private function CheckCardPlayable(player:PatruPlayer, card:String):Boolean 
		{
			var _canBePlayed:Boolean;
			
			//--------------------
			//		BAD CARDS
			//--------------------
			
			//GIVE CARD
			if 		(card == CARD_GIVE_CARD)
			{
				if (player.hand.length > 1 && player.opponent.hand.length < 4) _canBePlayed = true;
			}
			//LOSE A TURN
			else if (card == CARD_LOSE_NEXT_TURN)
			{
				_canBePlayed = true;
			}
			//DISCARD HAND
			else if (card == CARD_DISCARD_HAND)
			{
				_canBePlayed = true;
			}
			//LOSE RANDOM PIECE
			else if (card == CARD_LOSE_RANDOM_PIECE)
			{
				if (player.numPieces > 0) _canBePlayed = true;
			}
			
			//--------------------
			//		GOOD CARDS
			//--------------------
			
			//REROLL
			if 		(card == CARD_REROLL)
			{
				_canBePlayed = player.canReroll;
			}
			//REMOVE PIECE
			else if (card == CARD_REMOVE_PIECE)
			{
				if (player.opponent.numPieces > 0) _canBePlayed = true;
			}
			//ADD PIECE
			else if (card == CARD_ADD_PIECE)
			{
				_canBePlayed = true;
			}
			//STEAL CARD
			else if (card == CARD_STEAL_CARD)
			{
				if (player.opponent.hand.length > 0) _canBePlayed = true;
			}
			//EXTRA TURN
			else if (card == CARD_EXTRA_TURN)
			{
				_canBePlayed = true;
			}
			//BATTLE
			else if (card == CARD_BATTLE)
			{
				_canBePlayed = true;
			}
			
			return _canBePlayed;
		}
		
		/**
		 * (DEPRECATED UNTIL BUG IS FIXED: INCORRECTLY REPORTS A WIN SOMETIMES)
		 * 
		 * Checks for a winner by systematically searching the board for any case of four-in-a-row.
		 */
		private function CheckWinNEW(clickedSquare:MovieClip, clickingPlayer:PatruPlayer):void 
		{
			//Setup Variables
			var XOffSet:int;
			var YOffSet:int;
			var WinState:int;
			
			var _currentSquare:MovieClip;
			
			//Needs to check in two direction at a time.  Up/down, UpRight/DownLeft, Right/Left, DownRight/UpLeft
			///Increment direction and set Offsets
			for (var i:int = 1; i < 9; i++)
			{
				//Up
				if (i == 1)
				{
					WinState = 1;
					
					XOffSet = 0;
					YOffSet = -1;
				}
				//Down
				else if (i == 2)
				{
					XOffSet = 0;
					YOffSet = 1;
				}
				//UpRight
				else if (i == 3)
				{
					WinState = 1;
					
					XOffSet = 1;
					YOffSet = -1;
				}
				//DownLeft
				else if (i == 4)
				{
					XOffSet = -1;
					YOffSet = 1;
				}
				//Right
				else if (i == 5)
				{
					WinState = 1;
					
					XOffSet = 1;
					YOffSet = 0;
				}
				//Left
				else if (i == 6)
				{
					XOffSet = -1;
					YOffSet = 0;
				}
				//DownRight
				else if (i == 7)
				{
					WinState = 1;
					
					XOffSet = 1;
					YOffSet = 1;
				}
				//UpLeft
				else
				{
					XOffSet = -1;
					YOffSet = -1;
				}
				
				//Increment position checked
				for (var j:int = 1; j < 4; j++)
				{
					_currentSquare = null;
					
					try
					{
						_currentSquare = this["board" + (clickedSquare.col + XOffSet * j) + (clickedSquare.row + YOffSet * j)];						
					}
					catch (err:Error)
					{
						
					}
					
					if (_currentSquare && _currentSquare.currentFrame == clickingPlayer.pieceFrame)
					{
						WinState++;
					}
					else if (_currentSquare && _currentSquare.currentFrame == clickingPlayer.opponent.pieceFrame)
					{
						j = 5;
					}
					
					//Check for a win
					if (WinState == 4)
					{
						OpenGameOver(clickedSquare.currentFrame);
					}
					//Check for tie
					else if (_p1.numPieces + _p2.numPieces == 36)
					{
						OpenGameOver( -1);
					}
				}
			}
		}
		
		/**
		 * (PREVIOUSLY DEPRECATED) Checks for a winner by systematically searching the board for any case of four-in-a-row.
		 */
		private function CheckWin(clickedSquare:MovieClip, clickingPlayer:PatruPlayer):void 
		{
			var _winnerFound:Boolean;
			
			//Loop through every square, checking 4 pieces in 4 different directions in that square.
			outerLoop: for (var i:int = 1; i < 7; i++)
			{
				for (var j:int = 1; j < 7; j++)
				{
					var _startSquare:MovieClip = this["board" + j + i];	//set this square to variable for brevity
					
					//Only check if square isn't blank
					if (_startSquare.currentLabel != "blank")
					{	
						//CHECK 4 RIGHT, make sure all 4 exist and check for matches at the same time
						if 		(j <= 3 &&
								(this["board" + (j + 1) + i] && this["board" + (j + 1) + i].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j + 2) + i] && this["board" + (j + 2) + i].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j + 3) + i] && this["board" + (j + 3) + i].currentFrame == _startSquare.currentFrame))
						{
							_winnerFound = true;
							break outerLoop;
						}
						//CHECK 4 DOWN RIGHT, make sure all 4 exist and check for matches at the same time
						else if (j <= 3 && i <= 3 &&
								(this["board" + (j + 1) + (i + 1)] && this["board" + (j + 1) + (i + 1)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j + 2) + (i + 2)] && this["board" + (j + 2) + (i + 2)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j + 3) + (i + 3)] && this["board" + (j + 3) + (i + 3)].currentFrame == _startSquare.currentFrame))
						{
							_winnerFound = true;
							break outerLoop;
						}
						//CHECK 4 DOWN, make sure all 4 exist and check for matches at the same time
						else if (i <= 3 &&
								(this["board" + j + (i + 1)] && this["board" + j + (i + 1)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + j + (i + 2)] && this["board" + j + (i + 2)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + j + (i + 3)] && this["board" + j + (i + 3)].currentFrame == _startSquare.currentFrame))
						{
							_winnerFound = true;
							break outerLoop;
						}
						//CHECK 4 DOWN LEFT, make sure all 4 exist and check for matches at the same time
						else if (j >= 4 && i <= 3 &&
								(this["board" + (j - 1) + (i + 1)] && this["board" + (j - 1) + (i + 1)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j - 2) + (i + 2)] && this["board" + (j - 2) + (i + 2)].currentFrame == _startSquare.currentFrame) &&
								(this["board" + (j - 3) + (i + 3)] && this["board" + (j - 3) + (i + 3)].currentFrame == _startSquare.currentFrame))
						{
							_winnerFound = true;
							break outerLoop;
						}
						else continue;	//if none are a full match, continue to next square
					}
				}
			}
			
			//If a winner was found, find out who won, based on the start square
			if (_winnerFound)
			{		
				OpenGameOver(_startSquare.currentFrame);
			}
			//If not, check for a tie
			else if (_p1.numPieces + _p2.numPieces == 36)	//36 pieces means board is full
			{
				OpenGameOver(-1);
			}
		}
		
		/**
		 * Loops through all squares and clears their yellow highlights, thereby disabling them.
		 * 
		 * @param	clearPieces		If true, removes all pieces from the board in addition to disabling the squares.
		 */
		private function DisableBoard(clearPieces:Boolean):void 
		{
			for (var i:int = 1; i < 7; i++)
			{
				for (var j:int = 1; j < 7; j++)
				{
					DisableSquare(this["board" + j + i] as MovieClip);
					if (clearPieces)
					{
						this["board" + j + i].gotoAndStop("blank");
						this["board" + j + i].filled = false;
					}
				}
			}
		}
		
		/**
		 * Disallows a specific card slot from being clicked and examined.
		 * 
		 * @param	slot	The card slot that should be disabled.
		 */
		private function DisableCardSlot(slot:MovieClip):void 
		{
			slot.gotoAndStop(1);		//empty
			slot.buttonMode = false;
			slot.removeEventListener(MouseEvent.CLICK, HandleCardSlotClicked);
		}
		
		/**
		 * Disallows a specific square from being clicked.
		 * 
		 * @param	square	The square that should be disabled.
		 */
		private function DisableSquare(square:MovieClip):void 
		{
			square.highlighter.visible = false;
			square.buttonMode = false;
			square.removeEventListener(MouseEvent.CLICK, HandleSquareClicked);
		}
		
		/**
		 * Discards a specific card from a player's hand, places that card back into the deck, and updates their hand display if needed.
		 * 
		 * @param	player		The player whose card should be discarded.
		 * @param	card		The card that should be discarded from <code>player</code>'s hand.
		 * @param	showHand	Whether or not that player's hand should now be displayed. Often <code>false</code> if discarding from non-turn player's hand.
		 */
		private function DiscardCard(player:PatruPlayer, card:String, showHand:Boolean):void 
		{
			//Search the player's hand for the specified card and remove it
			if (player.hand.indexOf(card) > -1)
			{
				player.hand.splice(player.hand.indexOf(card), 1);
				
				//Put that card back into the deck
				_cardDeck.push(card);
				
				//If the card was a Shield, decrement player's shield count
				if (card == CARD_SHIELD) player.numShields--;
			}
			
			//Display the new hand if needed
			if (showHand) ShowHand(player);
		}
		
		/**
		 * Splices a random element from the current deck of cards and adds that to the turn player's hand.
		 * 
		 * @param	event	The optional event object that holds the event.
		 */
		private function DrawCard(event:MouseEvent = null):void 
		{
			btnDraw.visible = false;
			
			//Choose a random card from the deck
			var _randCardIndex:int = Math.random() * _cardDeck.length;
			var _drawnCard:String = _cardDeck[_randCardIndex];
			_cardDeck.splice(_randCardIndex, 1);
			
			//Add that card to the turn player's hand
			_turnPlayer.hand.push(_drawnCard);
			
			//Disallow player from playing the Reroll card since they took an action this turn
			_turnPlayer.canReroll = false;
			
			//Update the display to show the card in a slot
			ShowHand(_turnPlayer);
			
			//If the card is bad, it has to be examined immediately
			if (_cardsNegative.indexOf(_drawnCard) > -1)
			{
				//First, check to see if the card can even be played at all
				var _canBePlayed:Boolean = CheckCardPlayable(_turnPlayer, _drawnCard);
				
				//Then show card dialog based on results
				OpenCardDialog(_drawnCard, _canBePlayed, _canBePlayed);
			}
			//If the card is Shield, increase player's shield count
			else if (_drawnCard == CARD_SHIELD)
			{
				_turnPlayer.numShields++;
			}
		}
		
		/**
		 * Allows a specific card slot to be clicked and examined.
		 * 
		 * @param	slot	The card slot that should be enabled.
		 */
		private function EnableCardSlot(slot:MovieClip):void 
		{
			slot.buttonMode = true;
			slot.addEventListener(MouseEvent.CLICK, HandleCardSlotClicked);
		}
		
		/**
		 * Allows a specific square to be clicked, and shows its yellow highlight.
		 * 
		 * @param	square	The square that should be enabled.
		 */
		private function EnableSquare(square:MovieClip):void 
		{
			square.highlighter.visible = true;
			square.buttonMode = true;
			square.addEventListener(MouseEvent.CLICK, HandleSquareClicked);
		}
		
		/**
		 * Ends the current turn and switches to the non-turn player, or asks the turn player if they are sure they want to end turn if they still have a chance to
		 * place a piece or draw a card this turn.
		 * 
		 * @param	event	The optional event object that holds the event.
		 */
		private function EndTurn(event:MouseEvent = null):void 
		{
			//Search the entire board for highlighted squares
			var _activeSquare:Boolean;
			outerLoop: for (var i:int = 1; i < 7; i++)
			{
				for (var j:int = 1; j < 7; j++)
				{
					if (this["board" + j + i].highlighter.visible)
					{
						_activeSquare = true;
						break outerLoop;
					}
				}
			}
			
			if (btnDraw.visible || btnRoll.visible || _activeSquare) OpenEndTurnDialog();
			else SwitchToPlayer(_nonTurnPlayer, true, true);
		}
		
		/**
		 * Serves as a "buffer" time between AI actions, which slows down its pace and prevents it from completing a whole turn almost instantly. When this buffer
		 * time is complete, the AI can move on to its new task.
		 */
		private function HandleAIThinkingComplete():void 
		{
			
		}
		
		/**
		 * Allows the winner of the Battle card effect to choose a piece of their choice.
		 * 
		 * @param	event	The event object that holds the winner of the battle.
		 */
		private function HandleBattleComplete(event:PatruEvent):void 
		{
			_battleWinner = PatruPlayer(event.data);
			
			//Highlight all board squares except winner's filled squares
			for (var i:int = 1; i < 7; i++)
			{
				for (var j:int = 1; j < 7; j++)
				{
					if (this["board" + j + i].currentFrame != _battleWinner.pieceFrame) EnableSquare(this["board" + j + i]);
				}
			}
		}
		
		/**
		 * Shows a close-up explanation of a specific card, and allows it to be played if possible.
		 * 
		 * @param	event	The event object that holds the card that was clicked.
		 */
		private function HandleCardSlotClicked(event:MouseEvent):void 
		{
			var _thisCard:String = event.currentTarget.currentLabel;
			var _mustBePlayed:Boolean;
			var _canBePlayed:Boolean = CheckCardPlayable(_turnPlayer, _thisCard);
			
			//First, check to see if the card can even be played at all
			if (_canBePlayed)
			{
				//Then determine whether or not the card MUST be played (negative cards only)
				if (_cardsNegative.indexOf(_thisCard) > -1 ) _mustBePlayed = true;
			}
			
			//Show card dialog based on results
			OpenCardDialog(_thisCard, _canBePlayed, _mustBePlayed);
		}
		
		/**
		 * Causes the card that is passed in the event object to be played.
		 * 
		 * @param	event	The event object that holds the card that should be played.
		 */
		private function HandleClickPlayCard(event:PatruEvent):void 
		{
			PlayCard(_turnPlayer, String(event.data));
			
			CloseScreen(null, _screenCardDialog);
		}
		
		/**
		 * Switches to the next player after the current player clicks the End Turn button in the end turn dialog, thereby confirming that they want to end their turn.
		 * 
		 * @param	event	The event object that holds the event.
		 */
		private function HandleEndTurnConfirmed(event:Event):void 
		{
			CloseScreen(null, _screenEndTurnDialog);
			
			SwitchToPlayer(_nonTurnPlayer, true, true);
		}
		
		/**
		 * Causes the dice to show the numbers that they rolled when their roll animation is complete.
		 */
		private function HandleDieRollComplete():void 
		{
			die1.gotoAndStop(_roll1);
			die2.gotoAndStop(_roll2);
		}
		
		/**
		 * Determines whether the player selected a 1-player game or a 2-player game and continues starting the game from there.
		 * 
		 * @param	event	The event object that holds the event.
		 */
		private function HandleModeSelected(event:Event):void 
		{
			//Create P1
			_p1 = new PatruPlayer();
			_p1.cardSlots = [p1Card1, p1Card2, p1Card3, p1Card4];
			
			//Create P2 based on which mode button was clicked
			if 		(event.type == PatruEvent.CLICK_1_PLAYER_GAME)		_p2 = new PatruAI(this);
			else if (event.type == PatruEvent.CLICK_2_PLAYER_GAME)		_p2 = new PatruPlayer();
			_p2.cardSlots = [p2Card1, p2Card2, p2Card3, p2Card4];
			
			_p1.opponent = _p2;
			_p2.opponent = _p1;
			
			//Open the theme selection screen now that the mode has been chosen
			OpenThemeSelect();
		}
		
		/**
		 * Places or removes a piece on the board, then checks for winners.
		 * 
		 * @param	event	The event object that holds the event.
		 */
		private function HandleSquareClicked(event:MouseEvent):void 
		{
			var _clickedSquare:PatruBoardSquare = PatruBoardSquare(event.currentTarget);
			
			//If removing a piece with Remove Piece card...
			//if (gameState == GAME_REMOVING_PIECE && _clickedSquare.currentLabel != "blank")
			if (gameState == GAME_REMOVING_PIECE && _clickedSquare.filled)
			{
				_clickedSquare.filled = false;
				
				_nonTurnPlayer.numPieces--;
				
				DisableBoard(false);
				
				//As long as a normal piece was not placed this turn, re-highlight the rolled squares if they're empty
				if (!_placedNormalPieceThisTurn)
				{
					//if (_firstSquare && _firstSquare.currentLabel == "blank") EnableSquare(_firstSquare);
					if (_firstSquare && !_firstSquare.filled) EnableSquare(_firstSquare);
					//if (_secondSquare && _secondSquare.currentLabel == "blank") EnableSquare(_secondSquare);
					if (_secondSquare && !_secondSquare.filled) EnableSquare(_secondSquare);
				}
				
				//Have the piece animate out of existence
				_clickedSquare.tweenPieceOut();
				
				SetGameState(GameBase.GAME_PLAYING);
			}
			//Otherwise, if adding a piece with Add Piece card...
			else if (gameState == GAME_ADDING_PIECE)
			{
				_clickedSquare.filled = true;
				
				_turnPlayer.numPieces++;
				
				DisableBoard(false);
				
				//As long as a normal piece was not placed this turn, re-highlight the rolled squares if they're empty
				if (!_placedNormalPieceThisTurn)
				{
					//if (_firstSquare && _firstSquare.currentLabel == "blank") EnableSquare(_firstSquare);
					if (_firstSquare && !_firstSquare.filled) EnableSquare(_firstSquare);
					//if (_secondSquare && _secondSquare.currentLabel == "blank") EnableSquare(_secondSquare);
					if (_secondSquare && !_secondSquare.filled) EnableSquare(_secondSquare);
				}
				
				//Have the piece animate into existence
				_clickedSquare.gotoAndStop(_turnPlayer.pieceFrame);
				_clickedSquare.tweenPieceIn();
				
				SetGameState(GameBase.GAME_PLAYING);
				
				//CheckWin();
				CheckWin(_clickedSquare, _turnPlayer);
			}
			//Otherwise, if adding or converting with Battle card...
			else if (gameState == GAME_BATTLING)
			{
				_clickedSquare.filled = true;
				
				//If square was stolen, decrement battle winner's opponent's piece count
				if (_clickedSquare.currentFrame == _battleWinner.opponent.pieceFrame) _battleWinner.opponent.numPieces--;
				
				_battleWinner.numPieces++;
				
				DisableBoard(false);
				
				//As long as a normal piece was not placed this turn, re-highlight the rolled squares if they're empty
				if (!_placedNormalPieceThisTurn)
				{
					//if (_firstSquare && _firstSquare.currentLabel == "blank") EnableSquare(_firstSquare);
					if (_firstSquare && !_firstSquare.filled) EnableSquare(_firstSquare);
					//if (_secondSquare && _secondSquare.currentLabel == "blank") EnableSquare(_secondSquare);
					if (_secondSquare && !_secondSquare.filled) EnableSquare(_secondSquare);
				}
				
				//Have the piece animate into existence
				_clickedSquare.gotoAndStop(_battleWinner.pieceFrame);
				_clickedSquare.tweenPieceIn();
				
				SetGameState(GameBase.GAME_PLAYING);
				
				//If it's the AI's turn, tell it that it can continue its turn now
				if (_turnPlayer == _p2 && _battleWinner != _p2 && _p2 is PatruAI)
				{
					PatruAI(_p2).continueAfterBattle();
				}
				
				//CheckWin();
				CheckWin(_clickedSquare, _battleWinner);
				
				_battleWinner = null;
			}
			//Otherwise, if adding a piece normally...
			else if (gameState == GameBase.GAME_PLAYING)
			{
				_clickedSquare.filled = true;
				
				_turnPlayer.numPieces++;								//increment clicker's piece count for tie check
				_turnPlayer.canReroll = false;							//player can no longer play Reroll card this turn
				
				_placedNormalPieceThisTurn = true;
				
				DisableBoard(false);
				
				//Have the piece animate into existence
				_clickedSquare.gotoAndStop(_turnPlayer.pieceFrame);
				_clickedSquare.tweenPieceIn();
				
				//CheckWin();
				CheckWin(_clickedSquare, _turnPlayer);
			}
		}
		
		/**
		 * Determines which theme has been selected and proceeds with starting a new game.
		 * 
		 * @param	event	The event object that holds the event.
		 */
		private function HandleThemeSelected(event:Event):void 
		{
			turnPlayerIndicator.gotoAndStop(_screenThemeSelect.selectedTheme);
			
			_currentTheme = turnPlayerIndicator.currentFrame;
			
			_p1.pieceFrame = _currentTheme;
			_p2.pieceFrame = _currentTheme + 1;	//p2's pieces are always 1 frame after p1's in the timeline
			
			StartGame();
		}
		
		/**
		 * Activates the effect of a specific card, played by a specific player.
		 * 
		 * @param	player	The player that is activating the card.
		 * @param	card	The card to be activated.
		 */
		private function PlayCard(player:PatruPlayer, card:String):void 
		{
			var i:int;
			var j:int;
			
			//Discard the card immediately
			DiscardCard(player, card, (player == _turnPlayer));		//only update hand display if player is turn player
			
			//--------------------
			//		BAD CARDS
			//--------------------
			
			//GIVE CARD
			if 		(card == CARD_GIVE_CARD)
			{
				//Stop card effect if shielded
				if (player.numShields > 0)
				{
					DiscardCard(player, CARD_SHIELD, (player == _turnPlayer));
					OpenShielded();
				}
				else
				{
					//Choose a random card from the player's hand
					var _randCardIndex:int = Math.random() * player.hand.length;
					var _drawnCard:String = player.hand[_randCardIndex];
					DiscardCard(player, _drawnCard, true);
					_cardDeck.splice(_cardDeck.indexOf(_drawnCard), 1);		//remove the card from the deck (gets added upon discard)
					
					//Add that card to the player's opponent's hand
					player.opponent.hand.push(_drawnCard);
				}
			}
			//LOSE A TURN
			else if (card == CARD_LOSE_NEXT_TURN)
			{
				//Stop card effect if shielded
				if (player.numShields > 0)
				{
					DiscardCard(player, CARD_SHIELD, (player == _turnPlayer));
					OpenShielded();
				}
				else
				{
					player.skipTurnCount++;					
				}
			}
			//DISCARD HAND
			else if (card == CARD_DISCARD_HAND)
			{
				//Stop card effect if shielded
				if (player.numShields > 0)
				{
					DiscardCard(player, CARD_SHIELD, (player == _turnPlayer));
					OpenShielded();
				}
				else
				{
					for (i = player.hand.length - 1; i >= 0; i--)
					{
						DiscardCard(player, player.hand[i], (player == _turnPlayer));	//only update hand display if player is turn player
					}
				}
			}
			//LOSE RANDOM PIECE
			else if (card == CARD_LOSE_RANDOM_PIECE)
			{
				//Stop card effect if shielded
				if (player.numShields > 0)
				{
					DiscardCard(player, CARD_SHIELD, (player == _turnPlayer));
					OpenShielded();
				}
				else
				{
					//Make a list of all of the player's pieces
					var _pieceList:Array = [];
					for (i = 1; i < 7; i++)
					{
						for (j = 1; j < 7; j++)
						{
							if (this["board" + j + i].currentFrame == _turnPlayer.pieceFrame) _pieceList.push(this["board" + j + i]);
						}
					}
					
					//Randomly select one of those pieces and remove it
					var _randPiece:int = Math.random() * _pieceList.length;
					//_pieceList[_randPiece].gotoAndStop("blank");
					_pieceList[_randPiece].tweenPieceOut();
					_pieceList[_randPiece].filled = false;
					player.numPieces--;
				}
			}
			
			//--------------------
			//		GOOD CARDS
			//--------------------
			
			//REROLL
			else if	(card == CARD_REROLL)
			{
				//Clear any highlighted squares and the Draw Card button if needed
				DisableBoard(false);
				btnDraw.visible = false;
				
				RollDice();
			}
			//REMOVE PIECE
			else if (card == CARD_REMOVE_PIECE)
			{
				//Stop card effect if opponent shielded
				if (player.opponent.numShields > 0)
				{
					DiscardCard(player.opponent, CARD_SHIELD, (player == _nonTurnPlayer));
					OpenShielded();
				}
				else
				{
					//Search for all of opponent's pieces and highlight them
					for (i = 1; i < 7; i++)
					{
						for (j = 1; j < 7; j++)
						{
							if (this["board" + j + i].currentFrame == _nonTurnPlayer.pieceFrame)
							{
								EnableSquare(this["board" + j + i]);
							}
						}
					}
					
					SetGameState(GAME_REMOVING_PIECE);
				}
			}
			//ADD PIECE
			else if (card == CARD_ADD_PIECE)
			{
				//Highlight all open squares
				for (i = 1; i < 7; i++)
				{
					for (j = 1; j < 7; j++)
					{
						if (this["board" + j + i].currentLabel == "blank")
						{
							EnableSquare(this["board" + j + i]);
						}
					}
				}
				
				SetGameState(GAME_ADDING_PIECE);
			}
			//STEAL CARD
			else if (card == CARD_STEAL_CARD)
			{
				//Stop card effect if opponent shielded
				if (player.opponent.numShields > 0)
				{
					DiscardCard(player.opponent, CARD_SHIELD, (player == _nonTurnPlayer));
					OpenShielded();
				}
				else
				{
					//Choose a random card from the player's opponent's hand
					_randCardIndex = Math.random() * player.opponent.hand.length;
					_drawnCard = player.opponent.hand[_randCardIndex];
					DiscardCard(player.opponent, _drawnCard, false);
					_cardDeck.splice(_cardDeck.indexOf(_drawnCard), 1);		//remove the card from the deck (gets added upon discard)
					
					//Add that card to the player's hand
					player.hand.push(_drawnCard);
					
					//Update the display to show the card in a slot
					ShowHand(player);
				}
			}
			//EXTRA TURN
			else if (card == CARD_EXTRA_TURN)
			{
				player.extraTurnCount++;
			}
			//BATTLE
			else if (card == CARD_BATTLE)
			{
				OpenRollBattle();
				
				SetGameState(GAME_BATTLING);
			}			
		}
		
		/**
		 * Quits the game and returns to the main menu.
		 */
		private function QuitToMain(event:Event = null):void 
		{
			DisableBoard(true);
			CloseAllScreens();
			RemoveDynamicObjects();
			SetGameState(GameBase.GAME_OVER);
			OpenMainMenu();
		}
		
		/**
		 * Removes all objects that have been added dynamically, preparing them for garbage collection.
		 */
		private function RemoveDynamicObjects():void 
		{
			if (_p2 is PatruAI) PatruAI(_p2).killMe();
			
			_p1 = null;
			_p2 = null;
		}
		
		/**
		 * Animates the dice and highlights up to two squares on the board that correspond to the dice rolls. The player is then able to click one of the
		 * highlighted squares.
		 * 
		 * @param	event	The optional event object that holds the event.
		 * 
		 * @see 	HandleSquareClicked()
		 */
		private function RollDice(event:MouseEvent = null):void 
		{
			btnRoll.visible = false;				//hide Roll button, cannot roll twice in a row
			
			_turnPlayer.canReroll = true;			//player is allowed to play Reroll card now
			
			//Loop until at least one open spot is found OR the player rolls doubles
			while (true)
			{
				//Generates rolls
				_roll1 = Math.random() * 6 + 1;
				_roll2 = Math.random() * 6 + 1;
				//_roll2 = _roll1;		//testing purposes
				die1.gotoAndPlay(7);	//animation frame
				die2.gotoAndPlay(7);
				die1.addFrameScript(die1.totalFrames - 1, HandleDieRollComplete);
				
				//Assign rolled squares to variables for brevity
				_firstSquare = this["board" + _roll1 + _roll2];
				_secondSquare = this["board" + _roll2 + _roll1];
				
				//Determine roll outcome and perform appropriate action
				//Possible outcomes:	1. First square open, second square full: highlight first square
				//						2. Second square open, first square full: highlight second square
				//						3. Both open: highlight both squares
				//						4. Both full: continue loop
				//						5. Doubles, square full: allow player to draw card
				//						6. Doubles, square open: highlight square, allow player to draw card
				
				//First, check for doubles
				if (_roll1 == _roll2)
				{
					//Doubles, square open
					if (_firstSquare.currentLabel == "blank") EnableSquare(_firstSquare);
					
					//Allow player to draw if they don't have 4 cards already
					if (_turnPlayer.hand.length < 4) btnDraw.visible = true;		//show Draw button
					
					break;
				}
				//If no doubles, determine other outcome
				else
				{
					if (_firstSquare.currentLabel != "blank" && _secondSquare.currentLabel != "blank") continue;	//both full
					else
					{
						if 		(_firstSquare.currentLabel == "blank" && _secondSquare.currentLabel != "blank")  EnableSquare(_firstSquare);									//first open, second full
						else if (_firstSquare.currentLabel != "blank" && _secondSquare.currentLabel == "blank")  EnableSquare(_secondSquare);									//first full, second open
						else if (_firstSquare.currentLabel == "blank" && _secondSquare.currentLabel == "blank") { EnableSquare(_firstSquare); EnableSquare(_secondSquare); }	//both open
						
						break;
					}
				}
			}
			
			_se.playSound(SFX_DICE_ROLL_01);
		}
		
		/**
		 * Changes the overall state of the game, performing actions appropriate to the new state upon entering it. The only values passed should be game state
		 * constants, the primary of which can found in in the <code>GameBase</code> class.
		 * 
		 * @param	state	The new game state.
		 */
		private function SetGameState(state:int):void 
		{
			gameState = state;
			
			//Remove specialized game state effects (will be added back if needed)
			clickPreventerButtons.visible = clickPreventerCards.visible = clickPreventerBoard.visible = false;
			
			if		(gameState == GameBase.GAME_OVER)
			{
				RemoveInGameEventListeners();
			}
			else if (gameState == GameBase.GAME_PAUSED)
			{
				RemoveInGameEventListeners();
			}
			else if (gameState == GameBase.GAME_PLAYING)
			{
				AddInGameEventListeners();
			}
			else if (gameState == GAME_ADDING_PIECE)
			{
				addChild(clickPreventerButtons);
				clickPreventerButtons.visible = true;	//disallow clicking on buttons or cards while in this state
				addChild(clickPreventerCards);
				clickPreventerCards.visible = true;
			}
			else if (gameState == GAME_BATTLING)
			{
				addChild(clickPreventerButtons);
				clickPreventerButtons.visible = true;	//disallow clicking on buttons or cards while in this state
				addChild(clickPreventerCards);
				clickPreventerCards.visible = true;
			}
			else if (gameState == GAME_REMOVING_PIECE)
			{
				addChild(clickPreventerButtons);
				clickPreventerButtons.visible = true;	//disallow clicking on buttons or cards while in this state
				addChild(clickPreventerCards);
				clickPreventerCards.visible = true;
			}
		}
		
		/**
		 * Makes visible a specific player's hand. Useful when switching players or when a player draws a card.
		 * 
		 * @param	player
		 */
		private function ShowHand(player:PatruPlayer):void 
		{
			//Loop through all of the player's card objects and make them visible, as well as display the appropriate card in each
			var _thisSlot:MovieClip;
			for (var i:int = 0; i < player.cardSlots.length; i++)
			{
				_thisSlot = player.cardSlots[i];	//for brevity/readability
				
				//Make sure opponent's cards are behind these cards
				if (player == _p1) _thisSlot.visible = true;
				else _p1.cardSlots[i].visible = false;
				
				//Have the card go to the appropriate frame
				_thisSlot.gotoAndStop(player.hand[i]);
				
				//Allow the active slots to be clicked
				if (_thisSlot.currentFrame > 1) EnableCardSlot(_thisSlot);
				//Disallow the inactive card slots from being clicked
				else DisableCardSlot(_thisSlot);
			}
		}
		
		/**
		 * Change to a different player's turn.
		 * 
		 * @param	player				The player whose turn it should now become.
		 * @param	checkSkippedTurns	Whether or not this player's skip turn count should be considered (switches to the opposite player and decrements that counter if so).
		 * @param	checkExtraTurns		Whether or not the turn player's extra turns should be considered (remains on turn player's turn and decrements that counter if so).
		 */
		private function SwitchToPlayer(player:PatruPlayer, checkSkippedTurns:Boolean, checkExtraTurns:Boolean):void 
		{
			//If considering skipped turns and extra turns, deal with those first
			if (checkExtraTurns)
			{
				if (player.opponent.extraTurnCount > 0)					//if turn player still has an extra turn...
				{
					player.opponent.extraTurnCount--;					//decrement extra turn count
					SwitchToPlayer(player.opponent, false, false);		//and keep it on his turn
					
					return;
				}
			}
			
			if (checkSkippedTurns)
			{
				if (player.skipTurnCount > 0)
				{
					player.skipTurnCount--;		//decrement skip turn count
					
					//Only skip the player if they don't have an extra turn
					if (player.extraTurnCount > 0)	player.extraTurnCount--;	//if they have an extra turn, use one up and it can now become their turn
					else
					{
						SwitchToPlayer(player.opponent, true, true);			//if they don't have one, their turn is skipped
						
						return;
					}
				}
			}
			
			//Make the switch!
			_turnPlayer = player;
			_nonTurnPlayer = player.opponent;
			
			_firstSquare = null;
			_secondSquare = null;
			btnRoll.visible = true;
			btnDraw.visible = false;
			_turnPlayer.canReroll = false;
			_nonTurnPlayer.canReroll = false;
			_placedNormalPieceThisTurn = false;
			DisableBoard(false);
			CloseScreen(null, _screenAIStatus);
			ShowHand(_turnPlayer);
			turnPlayerIndicator.gotoAndStop(_turnPlayer.pieceFrame);
			turnPlayerIndicator.tweenPieceIn();
			
			//If new turn player is AI, immediately have it start "thinking" about what to do next
			if (_turnPlayer is PatruAI)
			{
				OpenAIConsole();
				PatruAI(_turnPlayer).statusScreen = _screenAIStatus;
				PatruAI(_turnPlayer).beginTurn();
			}
		}
		
		
		/*/////////////////////////////////
		 * 
		 * 
		 * 			GAME SCREENS
		 * 
		 * 
		 */////////////////////////////////
		
		/**
		 * Closes all screens and dialogs in this game. Useful when a drastic game state change occurs, such as quitting to the main menu or exiting the game altogether.
		 * Can be used even if not every screen or dialog is currently visible.
		 * 
		 * @see		CloseScreen()
		 */
		private function CloseAllScreens():void 
		{
			CloseScreen(null, _screenAIStatus);
			CloseScreen(null, _screenCardDialog);
			CloseScreen(null, _screenCredits);
			CloseScreen(null, _screenEndTurnDialog);
			CloseScreen(null, _screenGameOver);
			CloseScreen(null, _screenHelp);
			CloseScreen(null, _screenMainMenu);
			CloseScreen(null, _screenRollBattle);
			CloseScreen(null, _screenShielded);
			CloseScreen(null, _screenThemeSelect);
		}
		
		/**
		 * Closes a specific screen or dialog in this game. If that screen or dialog does not exist, then calling this method will have no effect.
		 * 
		 * @param	event	If a screen dispatches an event in an attempt to close itself, it will automatically be passed to this method as the event object.
		 * The method can then reference the screen object directly in order to remove any listeners that may be attached to it.
		 * @param	screen	If attempting to close the screen through other means (such as through the <code>CloseAllScreens</code> method), a specific screen object can be passed,
		 * while the event object should be left null.
		 * 
		 * @see		CloseAllScreens()
		 */
		private function CloseScreen(event:Event = null, screen:MovieClip = null):void 
		{
			var _thisScreen:MovieClip;
			
			if (event) 			_thisScreen = event.target as MovieClip;
			else if (screen) 	_thisScreen = screen;
			
			if (_thisScreen && contains(_thisScreen))
			{
				_thisScreen.killMe();
				
				if 		(_thisScreen == _screenAIStatus)
				{
					_screenAIStatus = null;
				}
				else if	(_thisScreen == _screenCardDialog)
				{
					_screenCardDialog.removeEventListener(PatruEvent.CLICK_PLAY_CARD, HandleClickPlayCard);
					
					//If the card was negative, discard it from the player's hand
					if (_cardsNegative.indexOf(_screenCardDialog.targetCard) > -1)
					{
						DiscardCard(_turnPlayer, _screenCardDialog.targetCard, true);
					}
					
					_screenCardDialog = null;
				}
				else if (_thisScreen == _screenCredits)
				{
					_screenCredits = null;
				}
				else if (_thisScreen == _screenEndTurnDialog)
				{
					_screenEndTurnDialog.removeEventListener(PatruEvent.END_TURN_CONFIRMED, HandleEndTurnConfirmed);
					
					_screenEndTurnDialog = null;
				}
				else if (_thisScreen == _screenGameOver)
				{
					_screenGameOver.removeEventListener(PatruEvent.CLOSE_GAME_OVER, StartGame);
					
					_screenGameOver = null;
				}
				else if (_thisScreen == _screenHelp)
				{
					_screenHelp = null;
				}
				else if	(_thisScreen == _screenMainMenu)
				{
					_screenMainMenu.removeEventListener(PatruEvent.CLICK_1_PLAYER_GAME, HandleModeSelected);
					_screenMainMenu.removeEventListener(PatruEvent.CLICK_2_PLAYER_GAME, HandleModeSelected);
					_screenMainMenu.removeEventListener(GameEvent.CLICK_HELP, OpenHelp);
					_screenMainMenu.removeEventListener(GameEvent.CLICK_CREDITS, OpenCredits);
					
					_screenMainMenu = null;
				}
				else if (_thisScreen == _screenRollBattle)
				{
					_screenRollBattle.removeEventListener(PatruEvent.BATTLE_COMPLETE, HandleBattleComplete);
					
					_screenRollBattle = null;
					
					//If it's the AI's turn and a roll battle was just completed...
					if (_turnPlayer == _p2 && _p2 is PatruAI)
					{
						//If the AI won the roll, have it proceed with its turn normally
						if (_battleWinner == _p2)
						{
							PatruAI(_p2).continueAfterBattle();
						}
						//Otherwise, if P1 won the roll, have the AI wait for P1 to click a square
						else
						{
							_screenAIStatus.hideClickPrev();
						}
					}
				}
				else if (_thisScreen == _screenShielded)
				{
					_screenShielded = null;
				}
				else if	(_thisScreen == _screenThemeSelect)
				{
					_screenThemeSelect.removeEventListener(PatruEvent.CLICK_THEME_SELECT_BUTTON, HandleThemeSelected);
					
					_screenThemeSelect = null;
				}
			}
		}
		
		/**
		 * Displays a box that covers the cards, serving two purposes: (1) disallows P1 from seeing P2's hand, and (2) displays information about what P2 is doing
		 * during their turn. This box is only intended to be used for when P2 is an AI (i.e. during a single-player game).
		 */
		private function OpenAIConsole():void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenAIStatus) _screenAIStatus = new PatruScreenAIStatus();
			
			//If it exists, bring it to the top
			addChild(_screenAIStatus);
		}
		
		/**
		 * Opens a dialog box that displays a specific card and its description text. Allows the player examining the card to play the card if possible.
		 * 
		 * @param	targetCard		The specific card that should have its description displayed when the dialog opens.
		 * @param	canBePlayed		Whether or not the card can be played right away. Determined in the <code>HandleCardSlotClicked</code> method.
		 * @param	mustBePlayed	Whether or not the card must be played right away. Usually pertains to cards with negative effects. Determined in the <code>HandleCardSlotClicked</code> method.
		 * 
		 * @see		HandleCardSlotClicked()
		 */
		private function OpenCardDialog(targetCard:String, canBePlayed:Boolean, mustBePlayed:Boolean):void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenCardDialog) _screenCardDialog = new PatruScreenCardDialog(targetCard, canBePlayed, mustBePlayed);
			
			//If it exists, bring it to top
			addChild(_screenCardDialog);
			
			//Add listeners for screen's button clicks
			_screenCardDialog.addEventListener(PatruEvent.CLICK_PLAY_CARD, HandleClickPlayCard);
		}
		
		private function OpenCredits(event:Event = null):void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenCredits) _screenCredits = new PatruScreenCredits();
			
			//If it exists, bring it to top
			addChild(_screenCredits);
		}
		
		private function OpenEndTurnDialog():void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenEndTurnDialog) _screenEndTurnDialog = new PatruScreenEndTurnDialog();
			
			//If it exists, bring it to top
			addChild(_screenEndTurnDialog);
			
			//Add listeners for screen's button clicks
			_screenEndTurnDialog.addEventListener(PatruEvent.END_TURN_CONFIRMED, HandleEndTurnConfirmed);
		}
		
		/**
		 * Opens the game over screen, displaying the winner if there was a winner, or a tie notification in case of a tie.
		 * 
		 * @param	winnerPieceFrame	The frame that the winner piece icon in the game over screen should go to. If -1 is passed, it means the game was a tie.
		 */
		private function OpenGameOver(winnerPieceFrame:int):void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenGameOver) _screenGameOver = new PatruScreenGameOver(winnerPieceFrame);
			
			//If it exists, bring it to top
			addChild(_screenGameOver);
			
			//Add listeners for screen's button clicks
			_screenGameOver.addEventListener(PatruEvent.CLOSE_GAME_OVER, StartGame);
		}
		
		private function OpenHelp(event:Event = null):void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenHelp) _screenHelp = new PatruScreenHelp();
			
			//If it exists, bring it to top
			addChild(_screenHelp);
		}
		
		private function OpenMainMenu(event:Event = null):void 
		{
			CloseAllScreens();
			
			_screenMainMenu = new PatruScreenMainMenu();
			addChild(_screenMainMenu);
			
			//Add listeners for screen's button clicks
			_screenMainMenu.addEventListener(PatruEvent.CLICK_1_PLAYER_GAME, HandleModeSelected);
			_screenMainMenu.addEventListener(PatruEvent.CLICK_2_PLAYER_GAME, HandleModeSelected);
			_screenMainMenu.addEventListener(GameEvent.CLICK_HELP, OpenHelp);
			_screenMainMenu.addEventListener(GameEvent.CLICK_CREDITS, OpenCredits);
		}
		
		private function OpenRollBattle():void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenRollBattle) _screenRollBattle = new PatruScreenRollBattle(_p1, _p2);
			
			//If it exists, bring it to top
			addChild(_screenRollBattle);
			
			//Add listeners for screen's button clicks
			_screenRollBattle.addEventListener(PatruEvent.BATTLE_COMPLETE, HandleBattleComplete);
		}
		
		private function OpenShielded():void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenShielded) _screenShielded = new PatruScreenShielded();
			
			//If it exists, bring it to top
			addChild(_screenShielded);
		}
		
		private function OpenThemeSelect():void 
		{
			//If screen doesn't exist yet, create it
			if (!_screenThemeSelect) _screenThemeSelect = new PatruScreenThemeSelect();
			
			//If it exists, bring it to top
			addChild(_screenThemeSelect);
			
			//Add listeners for the screen's button clicks
			_screenThemeSelect.addEventListener(PatruEvent.CLICK_THEME_SELECT_BUTTON, HandleThemeSelected);
		}
		
		
		//GETTER/SETTERS (FOR USE WITH AI)
		public function get firstSquare():MovieClip  
		{
			return _firstSquare;
		}
		
		public function get secondSquare():MovieClip 
		{
			return _secondSquare;
		}
		
		public function get roll1():int 
		{
			return _roll1;
		}
		
		public function get roll2():int 
		{
			return _roll2;
		}
		
		public function get screenAIStatus():PatruScreenAIStatus 
		{
			return _screenAIStatus;
		}
		
		public function get screenCardDialog():PatruScreenCardDialog 
		{
			return _screenCardDialog;
		}
		
		public function get screenEndTurnDialog():PatruScreenEndTurnDialog 
		{
			return _screenEndTurnDialog;
		}
		
		public function get screenGameOver():PatruScreenGameOver 
		{
			return _screenGameOver;
		}
		
		public function get screenRollBattle():PatruScreenRollBattle 
		{
			return _screenRollBattle;
		}
		
		public function get screenShielded():PatruScreenShielded 
		{
			return _screenShielded;
		}
	}
}