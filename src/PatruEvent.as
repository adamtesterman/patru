package 
{
	import flash.events.Event;
	
	/**
	 * Custom event class built for the game "Patru." Allows arguments to be passed to the event object for use in event handler functions later.
	 * Also contains static constants for events that pertain only to the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruEvent extends Event
	{
		//PROPERTIES / Constants (Event Types)
		public static const AI_ANIMATION_COMPLETE:String =				"aiAnimationComplete";
		
		public static const BATTLE_COMPLETE:String =					"battleComplete";
		
		public static const CLICK_1_PLAYER_GAME:String =				"click1PlayerGame";
		public static const CLICK_2_PLAYER_GAME:String =				"click2PlayerGame";
		public static const CLICK_PLAY_CARD:String =					"clickPlayCard";
		public static const CLICK_THEME_SELECT_BUTTON:String =			"clickThemeSelectButton";
		
		public static const CLOSE_GAME_OVER:String =					"closeGameOver";
		public static const END_TURN_CONFIRMED:String =					"endTurnConfirmed";
		
		//PROPERTIES / Variables
		private var _data:*;
		
		
		//METHODS / Constructor
		public function PatruEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:* = null):void 
		{
			super(type, bubbles, cancelable);
			
			_data = data;
		}
		
		
		//GETTER/SETTERS
		public function get data():* 
		{
			return _data;
		}
		
		public function set data(value:*):void 
		{
			_data = value;
		}
	}
}