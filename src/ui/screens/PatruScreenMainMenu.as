package ui.screens
{
	import flash.display.*;
	import flash.events.*;
	
	import events.GameEvent;
	import ui.screens.ScreenBase;
	
	/**
	 * Main menu screen for the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenMainMenu extends ScreenBase
	{
		
		/**
		 * Creates a new main menu screen.
		 * 
		 */
		public function PatruScreenMainMenu():void
		{
			//New methods
			AddEventListeners();
			
			//Inherited methods
			bg.stop();
		}
		
		
		/**
		 * Prepares the screen for garbage collection.
		 * 
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
			
			if (parent) parent.removeChild(this);
		}
		
		
		/**
		 * Adds all event listeners for the screen's buttons.
		 * 
		 */
		private function AddEventListeners():void 
		{
			btn1PlayerGame.addEventListener(MouseEvent.CLICK, Handle1PlayerGameClicked);
			btn2PlayerGame.addEventListener(MouseEvent.CLICK, Handle2PlayerGameClicked);
			btnHelp.addEventListener(MouseEvent.CLICK, HandleHelpClicked);
			btnCredits.addEventListener(MouseEvent.CLICK, HandleCreditsClicked);
		}
		
		
		/**
		 * Removes all event listeners for the screen's buttons.
		 * 
		 */
		private function RemoveEventListeners():void 
		{
			btn1PlayerGame.removeEventListener(MouseEvent.CLICK, Handle1PlayerGameClicked);
			btn2PlayerGame.removeEventListener(MouseEvent.CLICK, Handle2PlayerGameClicked);
			btnHelp.removeEventListener(MouseEvent.CLICK, HandleHelpClicked);
			btnCredits.removeEventListener(MouseEvent.CLICK, HandleCreditsClicked);
		}
		
		
		private function Handle1PlayerGameClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(PatruEvent.CLICK_1_PLAYER_GAME));
		}
		
		private function Handle2PlayerGameClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(PatruEvent.CLICK_2_PLAYER_GAME));
		}
		
		private function HandleHelpClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLICK_HELP));
		}
		
		private function HandleCreditsClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(GameEvent.CLICK_CREDITS));
		}
	}
}