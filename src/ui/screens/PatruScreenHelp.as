package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * Help screen for the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenHelp extends ScreenBase
	{
		/**
		 * Creates a new help screen.
		 * 
		 */
		public function PatruScreenHelp():void
		{
			//New methods
			AddEventListeners();
			
			//Inherited properties
			btnPrev.visible = false;
			txtPageNum.text = String(currentFrame + "/" + totalFrames);
			
			//Inherited methods
			stop();
			bg.stop();
		}
		
		
		/**
		 * Prepares the screen for garbage collection.
		 * 
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
			
			if (parent) parent.removeChild(this);
		}
		
		
		/**
		 * Adds all event listeners for the screen's buttons.
		 * 
		 */
		private function AddEventListeners():void 
		{
			btnMainMenu.addEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnNext.addEventListener(MouseEvent.CLICK, NextPage);
			btnPrev.addEventListener(MouseEvent.CLICK, PrevPage);
		}
		
		
		/**
		 * Removes all event listeners for the screen's buttons.
		 * 
		 */
		private function RemoveEventListeners():void 
		{
			btnMainMenu.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnNext.removeEventListener(MouseEvent.CLICK, NextPage);
			btnPrev.removeEventListener(MouseEvent.CLICK, PrevPage);
		}
		
		/**
		 * Proceeds to the next page of the help screen, updating the next/previous buttons' visibility as needed.
		 * 
		 * @param	event	The optional <code>CLICK</code> MouseEvent object.
		 */
		private function NextPage(event:MouseEvent = null):void 
		{
			btnPrev.visible = true;
			
			nextFrame();
			
			txtPageNum.text = String(currentFrame + "/" + totalFrames);
			
			if (currentFrame == totalFrames) btnNext.visible = false;
			else btnNext.visible = true;
		}
		
		/**
		 * Proceeds to the previous page of the help screen, updating the next/previous buttons' visibility as needed.
		 * 
		 * @param	event	The optional <code>CLICK</code> MouseEvent object.
		 */
		private function PrevPage(event:MouseEvent = null):void 
		{
			btnNext.visible = true;
			
			prevFrame();
			
			txtPageNum.text = String(currentFrame + "/" + totalFrames);
			
			if (currentFrame == 1) btnPrev.visible = false;
			else btnPrev.visible = true;
		}
	}
}