package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * Theme select screen for the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenThemeSelect extends ScreenBase
	{
		//PROPERTIES / Variables
		private var _selectedTheme:String;
		
		//PROPERTIES / Lists
		private var _themeSelectButtons:Array = [];		//list of all theme select buttons on this screen
		
		
		/**
		 * Creates a new credits screen.
		 * 
		 */
		public function PatruScreenThemeSelect():void
		{
			//New methods
			FindThemeSelectButtons();
			AddEventListeners();
			
			//Inherited methods
			bg.stop();
		}
		
		
		/**
		 * Prepares the screen for garbage collection.
		 * 
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
			
			if (parent) parent.removeChild(this);
		}
		
		
		/**
		 * Adds all event listeners for the screen's buttons.
		 * 
		 */
		private function AddEventListeners():void 
		{
			btnMainMenu.addEventListener(MouseEvent.CLICK, handleCloseClicked);
			
			for (var i:int = 0; i < _themeSelectButtons.length; i++) 
			{
				_themeSelectButtons[i].addEventListener(MouseEvent.CLICK, HandleThemeSelectButtonClicked);
			}
		}
		
		
		/**
		 * Removes all event listeners for the screen's buttons.
		 * 
		 */
		private function RemoveEventListeners():void 
		{
			btnMainMenu.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
		}
		
		
		/**
		 * Searches the screen for instances of <code>ButtonThemeSelect</code> and adds them to a list.
		 * 
		 */
		private function FindThemeSelectButtons():void 
		{
			for (var i:int = 0; i < numChildren; i++) 
			{
				if (getChildAt(i) is ButtonThemeSelect)
				{
					_themeSelectButtons.push(ButtonThemeSelect(getChildAt(i)));
				}
			}
		}
		
		/**
		 * Determines which theme select button was clicked and sets the <code>_selectedTheme</code> property to its name for use outside the class.
		 * 
		 * @param	event	Holds the button that was clicked.
		 */
		private function HandleThemeSelectButtonClicked(event:MouseEvent):void 
		{
			//Randomly determine a theme if needed
			if (event.currentTarget.name == PatruSettings.THEME_RANDOM)
			{
				var _randIndex:int = Math.random() * PatruSettings.THEMES.length;
				_selectedTheme = PatruSettings.THEMES[_randIndex];
			}		
			else _selectedTheme = event.currentTarget.name;
			
			dispatchEvent(new Event(PatruEvent.CLICK_THEME_SELECT_BUTTON));
		}
		
		
		/* ////////////////////////////////
		 * 
		 * 
		 * 			GETTER/SETTERS
		 * 
		 * 
		 */////////////////////////////////
		public function get selectedTheme():String
		{
			return _selectedTheme;
		}
	}
}