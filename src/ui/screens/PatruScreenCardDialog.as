package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * Opens a dialog box that displays a specific card and its description text. Allows the player examining the card to play the card if possible.
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenCardDialog extends ScreenBase
	{
		//PROPERTIES / Variables
		private var _targetCard:String;		//the type of card that this is; corresponds with the frame labels of the <code>PatruScreenCardDialog</code> movie clip.
		
		/**
		 * Creates a new instance of <code>PatruScreenCardDialog</code>.
		 * 
		 * @param	targetCard		The specific card that should have its description displayed when the dialog opens.
		 * @param	canBePlayed		Whether or not the card can be played right away. Determines "Play" button's visibility.
		 * @param	mustBePlayed	Whether or not the card must be played right away. Usually pertains to cards with negative effects. Determine's the close button's visibility.
		 */
		public function PatruScreenCardDialog(targetCard:String, canBePlayed:Boolean, mustBePlayed:Boolean):void
		{
			//New properties
			_targetCard = targetCard;
			
			//Inherited properties
			if (!canBePlayed)	btnPlayCard.visible = false;
			if (mustBePlayed) 	btnClose.visible = false;			
			
			//New methods
			AddEventListeners();
			
			//Inherited methods
			gotoAndStop(targetCard);
			showClickPreventer(PatruSettings.RIGHT_EDGE, PatruSettings.BOTTOM_EDGE, 0, 0);
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void 
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void 
		{
			btnClose.addEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnPlayCard.addEventListener(MouseEvent.CLICK, HandleClickPlayCard);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void 
		{
			btnClose.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnPlayCard.removeEventListener(MouseEvent.CLICK, HandleClickPlayCard);
		}
		
		
		//Button click handlers
		private function HandleClickPlayCard(event:MouseEvent):void 
		{
			dispatchEvent(new PatruEvent(PatruEvent.CLICK_PLAY_CARD, false, false, _targetCard));
		}
		
		
		//GETTER/SETTERS
		public function get targetCard():String 
		{
			return _targetCard;
		}
		
		public function set targetCard(value:String):void 
		{
			_targetCard = value;
		}
	}
}