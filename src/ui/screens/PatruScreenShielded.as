package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PatruScreenShielded extends ScreenBase
	{
		//METHODS / Constructor
		public function PatruScreenShielded():void
		{
			//New methods
			AddEventListeners();
			
			//Inherited methods
			showClickPreventer(PatruSettings.RIGHT_EDGE, PatruSettings.BOTTOM_EDGE, 0, 0);
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void 
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void 
		{
			btnClose.addEventListener(MouseEvent.CLICK, handleCloseClicked);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void 
		{
			btnClose.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
		}
	}
}