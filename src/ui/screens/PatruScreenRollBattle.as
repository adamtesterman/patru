package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PatruScreenRollBattle extends ScreenBase
	{
		//PROPERTIES / Variables
		private var _roll1:int;
		private var _roll2:int;
		
		//PROPERTIES / Objects
		private var _p1:PatruPlayer; //references to the two players, so that a winner can be set
		private var _p2:PatruPlayer;
		private var _battleWinner:PatruPlayer;
		
		
		//METHODS / Constructor
		public function PatruScreenRollBattle(p1:PatruPlayer, p2:PatruPlayer):void
		{
			//New properties
			_p1 = p1;
			_p2 = p2;
			
			//New methods
			AddEventListeners();
			
			//Inherited properties
			btnClose.visible = false; 	//hide close button until rolls are complete
			btnRoll2.visible = false; 	//hide second Roll button until first roll is complete
			die1.scaleX = 1;
			die2.scaleX = -1;
			
			//Inherited methods
			showClickPreventer(PatruSettings.RIGHT_EDGE, PatruSettings.BOTTOM_EDGE, 0, 0);
			die1.stop();
			die2.stop();
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void
		{
			btnClose.addEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnRoll1.addEventListener(MouseEvent.CLICK, RollBattleDice);
			btnRoll2.addEventListener(MouseEvent.CLICK, RollBattleDice);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void
		{
			btnClose.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnRoll1.removeEventListener(MouseEvent.CLICK, RollBattleDice);
			btnRoll2.removeEventListener(MouseEvent.CLICK, RollBattleDice);
		}
		
		
		private function RollBattleDice(event:MouseEvent)
		{
			//Check if first roll has been completed
			if (_roll1 == 0)
			{
				//Generates rolls
				_roll1 = Math.random() * 6 + 1;
				die1.gotoAndPlay(7);	//animation frame
				die1.addFrameScript(die1.totalFrames - 1, HandleDieRollComplete);
				
				btnRoll1.visible = false;
				btnRoll2.visible = true;
			}
			//If first roll has been completed, move on to second roll
			else
			{
				//Keep looping until second roll does not equal first roll (i.e. avoid ties)
				while (true)
				{
					_roll2 = Math.random() * 6 + 1;
					if (_roll2 != _roll1) break;
				}
				
				die2.gotoAndPlay(7);	//animation frame
				die2.addFrameScript(die2.totalFrames - 1, HandleDieRollComplete);
				
				btnRoll2.visible = false;
				
				//Declare winner
				if (_roll1 > _roll2) 	_battleWinner = _p1;
				else					_battleWinner = _p2;
				
				btnClose.visible = true;
				
				dispatchEvent(new PatruEvent(PatruEvent.BATTLE_COMPLETE, false, false, _battleWinner));
			}
		}
		
		/**
		 * Causes the dice to show the numbers that they rolled when their roll animation is complete.
		 */
		private function HandleDieRollComplete():void 
		{
			die1.gotoAndStop(_roll1);
			die2.gotoAndStop(_roll2);
		}
	}
}