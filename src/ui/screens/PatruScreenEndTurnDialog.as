package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PatruScreenEndTurnDialog extends ScreenBase
	{
		//METHODS / Constructor
		public function PatruScreenEndTurnDialog():void
		{
			//New methods
			AddEventListeners();
			
			//Inherited methods
			showClickPreventer(PatruSettings.RIGHT_EDGE, PatruSettings.BOTTOM_EDGE, 0, 0);
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void 
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void 
		{
			btnClose.addEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnEndTurn.addEventListener(MouseEvent.CLICK, HandleEndTurnClicked);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void 
		{
			btnClose.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
			btnEndTurn.removeEventListener(MouseEvent.CLICK, HandleEndTurnClicked);
		}
		
		
		private function HandleEndTurnClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(PatruEvent.END_TURN_CONFIRMED));
		}
	}
}