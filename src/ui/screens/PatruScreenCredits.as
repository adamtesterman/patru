package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * Credits screen for the game "Patru."
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenCredits extends ScreenBase
	{
		/**
		 * Creates a new credits screen.
		 * 
		 */
		public function PatruScreenCredits():void
		{
			//New methods
			AddEventListeners();
			
			//Inherited methods
			bg.stop();
		}
		
		
		/**
		 * Prepares the screen for garbage collection.
		 * 
		 */
		public function killMe():void 
		{
			RemoveEventListeners();
			
			if (parent) parent.removeChild(this);
		}
		
		
		/**
		 * Adds all event listeners for the screen's buttons.
		 * 
		 */
		private function AddEventListeners():void 
		{
			btnMainMenu.addEventListener(MouseEvent.CLICK, handleCloseClicked);
		}
		
		
		/**
		 * Removes all event listeners for the screen's buttons.
		 * 
		 */
		private function RemoveEventListeners():void 
		{
			btnMainMenu.removeEventListener(MouseEvent.CLICK, handleCloseClicked);
		}
	}
}