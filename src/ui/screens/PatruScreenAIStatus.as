package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * Serves as a console through which P1 can read everything that P2 (the AI) is doing during its turn. Also hides P2's cards from
	 * P1.
	 * 
	 * @author Adam Testerman
	 */
	public class PatruScreenAIStatus extends ScreenBase
	{
		//METHODS / Constructor
		public function PatruScreenAIStatus():void
		{
			//Inherited methods
			AddEventListeners();
			showClickPrev();
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void 
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void 
		{
			addEventListener(Event.ENTER_FRAME, CheckFrame);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void 
		{
			removeEventListener(Event.ENTER_FRAME, CheckFrame);
		}
		
		
		private function CheckFrame(event:Event):void 
		{
			if (currentFrame % PatruSettings.AI_ANIMATION_LENGTH == 0)
			{
				stop();
				
				dispatchEvent(new Event(PatruEvent.AI_ANIMATION_COMPLETE));
			}
		}
		
		
		public function hideClickPrev():void 
		{
			hideClickPreventer();
		}
		
		public function showClickPrev(width:int = PatruSettings.RIGHT_EDGE, height:int = PatruSettings.BOTTOM_EDGE, x:int = 0, y:int = 0):void 
		{
			showClickPreventer(width, height, x, y);
			setClickPreventerAlpha(0);
		}
	}
}