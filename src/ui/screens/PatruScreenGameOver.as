package ui.screens
{
	import flash.events.*;
	
	import ui.screens.ScreenBase;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PatruScreenGameOver extends ScreenBase
	{
		//METHODS / Constructor
		public function PatruScreenGameOver(winnerPieceFrame:int):void
		{
			//New methods
			AddEventListeners();
			
			//Inherited methods
			stop();
			showClickPreventer(PatruSettings.RIGHT_EDGE, PatruSettings.BOTTOM_EDGE, 0, 0);
			if (winnerPieceFrame >= 0) winnerBox.gotoAndStop(winnerPieceFrame);
			else gotoAndStop("tie");	//if a negative winner frame was passed, it means it was a tie
		}
		
		
		//METHODS / Deconstructor
		public function killMe():void 
		{
			RemoveEventListeners();
			
			hideClickPreventer();
			
			if (parent) parent.removeChild(this);
		}
		
		
		//METHODS / Add Event Listeners
		private function AddEventListeners():void 
		{
			btnClose.addEventListener(MouseEvent.CLICK, HandleCloseClicked);
		}
		
		
		//METHODS / Remove Event Listeners
		private function RemoveEventListeners():void 
		{
			btnClose.removeEventListener(MouseEvent.CLICK, HandleCloseClicked);
		}
		
		
		private function HandleCloseClicked(event:MouseEvent):void 
		{
			dispatchEvent(new Event(PatruEvent.CLOSE_GAME_OVER));
		}
	}
}