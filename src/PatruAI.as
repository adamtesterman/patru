package 
{
	import flash.display.*;
	import flash.events.*;
	
	import ui.screens.PatruScreenAIStatus;
	
	/**
	 * The AI system used in "Patru" was created by Michael R. Stokes, Jr. The design incorporates four main structures:
	 * 
	 * 		1. Knowledge Discovery
	 * 		2. Experience Influence
	 * 		3. Trait Influence
	 * 		4. Decision Tree
	 * 
	 * @author Adam Testerman
	 */
	public class PatruAI extends PatruPlayer
	{
		//PROPERTIES / Constants
		private const STATUS_ANIM_LENGTH:int =							15;							//number of frames to "think" before moving on to next task
		
		private const STATE_ANALYZING_BOARD:String =					"analyzingBoard";			//state labels
		private const STATE_ANALYZING_CARD_DIALOG:String =				"analyzingCardDialog";
		private const STATE_ANALYZING_CARDS:String =					"analyzingCards";
		private const STATE_ANALYZING_OPTIONS:String =					"analyzingOptions";
		private const STATE_BATTLE_FINISHED:String =					"battleFinished";
		private const STATE_BATTLE_ROLLING:String =						"battleRolling";
		private const STATE_CHECKING_DRAW:String =						"checkingDraw";
		private const STATE_CHECKING_REROLL:String =					"checkingReroll";
		private const STATE_CHOOSING_CARD:String =						"choosingCard";
		private const STATE_CLICKING_SQUARE:String =					"clickingSquare";
		private const STATE_DRAWING_CARD:String =						"drawingCard";
		private const STATE_ENDING_TURN:String =						"endingTurn";
		private const STATE_READING_SHIELDED:String =					"readingShielded";
		private const STATE_ROLLING_DICE:String =						"rollingDice";
		private const STATE_STARTING_BATTLE:String =					"startingBattle";
		private const STATE_STARTING_TURN:String =						"startingTurn";
		
		private const FACTOR_PLAY_CARD_THEN_PLACE_PIECE:String =		"playCardThenPlayPiece";
		private const FACTOR_PLACE_PIECE_THEN_PLAY_CARD:String =		"placePieceThenPlayCard";
		private const FACTOR_CARD_1_USE:String =						"card1Use";
		private const FACTOR_CARD_2_USE:String =						"card2Use";
		private const FACTOR_CARD_3_USE:String =						"card3Use";
		private const FACTOR_CARD_4_USE:String =						"card4Use";
		private const FACTOR_CARD_1_DONT_USE:String =					"card1DontUse";
		private const FACTOR_CARD_2_DONT_USE:String =					"card2DontUse";
		private const FACTOR_CARD_3_DONT_USE:String =					"card3DontUse";
		private const FACTOR_CARD_4_DONT_USE:String =					"card4DontUse";
		
		private const INFLUENCE:int =									20;
		
		//PROPERTIES / Variables
		private var _state:String;										//holds whichever state of existence the AI is currently in
		private var _noPlayableCards:Boolean;							//determines whether or not there are any cards in hand that are playable (avoids an endless loop)
		
		private var _playCardThenPlacePiece:int;
		private var _placePieceThenPlayCard:int;
		
		/*private var _rerollUse:int;
		private var _rerollDontUse:int;
		private var _removePieceUse:int;
		private var _removePieceDontUse:int;
		private var _addPieceUse:int;
		private var _addPieceDontUse:int;
		private var _battleUse:int;
		private var _battleDontUse:int;*/
		
		private var _card1Use:int;
		private var _card2Use:int;
		private var _card3Use:int;
		private var _card4Use:int;
		private var _card1DontUse:int;
		private var _card2DontUse:int;
		private var _card3DontUse:int;
		private var _card4DontUse:int;
		
		//PROPERTIES / Lists
		private var _aiBoardValues:Array = [[], [], [], [], [], [], []];
		private var _aioBoardValues:Array = [[], [], [], [], [], [], []];
		
		//PROPERTIES / Objects
		private var _main:MovieClip;									//reference to document class
		
		//PROPERTIES / Screens
		private var _statusScreen:PatruScreenAIStatus;					//reference to the screen that controls the AI's "thinking" animations
		
		
		/**
		 * Constructor. Creates a new instance of <code>PatruAI</code>.
		 * 
		 * @param	main	Reference to the document class. Must be sent due to the AI's requirement to have knowledge about various parts of the game.
		 */
		public function PatruAI(main:MovieClip):void 
		{
			//New properties
			_state = STATE_STARTING_TURN;
			_main = main;
		}
		
		
		/**
		 * Deconstructor. Removes all dynamically-created objects and event listeners, preparing this object for garbage collection.
		 */
		public function killMe():void 
		{
			if (_statusScreen) _statusScreen.removeEventListener(PatruEvent.AI_ANIMATION_COMPLETE, ExecuteTasks);
		}
		
		
		/**
		 * Loops through the entire board, assigning values to each square, from both the perspective of the AI as well as from the
		 * perspective of the AI's opponent. In other words, each square essentially holds two values: one that tells us how valuable that square is from 
		 * the AI's point of view, and the other that tells us how valuable that square is from the AI's opponent's point of view.
		 */
		private function AnalyzeBoard():void 
		{
			var _thisSquare:PatruBoardSquare;
			
			for (var i:int = 1; i < 7; i++) 
			{
				for (var j:int = 1; j < 7; j++) 
				{
					//_aiBoardValues[i - 1][j - 1] = MapValues(_main["board" + i + j], PatruPlayer(this));
					//_aioBoardValues[i - 1][j - 1] = MapValues(_main["board" + i + j], opponent);
					
					//_main["board" + i + j].txtAIV.text = String(_aiBoardValues[i - 1][j - 1]);
					//_main["board" + i + j].txtAIO.text = String(_aioBoardValues[i - 1][j - 1]);
					
					_thisSquare = _main["board" + j + i];
					
					_thisSquare.aiValue = MapValues(_thisSquare, PatruPlayer(this));
					_thisSquare.aiOpponentValue = MapValues(_thisSquare, opponent);
				}
			}
		}
		
		/**
		 * Starts the AI on its information-gathering and decision-making for this turn.
		 */
		public function beginTurn():void 
		{
			_state = STATE_STARTING_TURN;
			
			_statusScreen.addEventListener(PatruEvent.AI_ANIMATION_COMPLETE, ExecuteTasks);
			
			ExecuteTasks();
		}
		
		/**
		 * Allows the AI to continue its turn after a "Battle" card's effect has concluded.
		 */
		public function continueAfterBattle():void 
		{
			_statusScreen.stop();
			
			_state = STATE_ANALYZING_BOARD;
			
			ExecuteTasks();
		}
		
		/**
		 * Performs an action that is relevant to this AI's state, followed by updating the status screen to display to P1 what is going on.
		 */
		private function ExecuteTasks(event:Event = null):void 
		{
			trace("AI state: " + _state);
			
			var i:int;
			var j:int;
			
			/*
				1	STATE_ROLLING_DICE				Roll dice, then go to 2.
				2	STATE_CHECKING_REROLL			If "Reroll" is in hand, decide whether or not to use it. If yes, open it and go to 6. If not, or if it's not in hand, go to 3.
				3	STATE_CHECKING_DRAW				If card can be drawn, go to 4. Otherwise, go to 5.
				4	STATE_DRAWING_CARD				Draw a card. If it's negative, go to 6. Otherwise, set NO PLAYABLE CARDS to false, then go to 5.
				5	STATE_ANALYZING_OPTIONS			Decide amongst playing card(s) in hand (go to 7), placing piece (go to 8), or ENDING TURN.
				6	STATE_ANALYZING_CARD_DIALOG		If card can be played, play it. If it was shielded, go to 10. If it's "Battle", go to 11. Otherwise, go to 3.
				7	STATE_ANALYZING_CARDS			"Think" about which card in hand to play. If none can be played, set NO PLAYABLE CARDS to true and go to 3. Otherwise, go to 9.
				8	STATE_ANALYZING_BOARD			"Think" about which piece to click, then go to 12.
				9	STATE_CHOOSING_CARD				Choose a card to play and open its dialog, then go to 6.
				10	STATE_READING_SHIELDED			Close "Shielded" dialog, then go to 6.
				11  STATE_STARTING_BATTLE			Wait for player to click Roll.
				12  STATE_CLICKING_SQUARE			Click the highest valued square, then go to 3.
			*/
			
			if 		(_state == STATE_STARTING_TURN)
			{
				//Execute task: Bide time.
				_statusScreen.gotoAndPlay("empty");
				
				//Next task: Roll dice.
				_state = STATE_ROLLING_DICE;
			}
			else if (_state == STATE_ROLLING_DICE)
			{
				//Execute task: Roll dice.
				_statusScreen.gotoAndPlay("rolling");
				_main.btnRoll.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				
				//Always get the current state of the board before moving on
				SetKnowledges();
				
				//Next task: Decide whether or not to use "Reroll."
				_state = STATE_CHECKING_REROLL;
			}
			else if (_state == STATE_CHECKING_REROLL)
			{
				//Execute task: Decide whether or not to use "Reroll."
				_statusScreen.gotoAndPlay("thinking");
				
				//Check if reroll is in hand
				var _rerollInHand:Boolean;
				for (i = 0; i < hand.length; i++)
				{
					if (hand[i] == "reroll")
					{
						_rerollInHand = true;
						var _rerollValues:Array = [		this["FACTOR_CARD_" + (i + 1) + "_USE"], 
														this["_card" + (i + 1) + "Use"],
														this["FACTOR_CARD_" + (i + 1) + "_DONT_USE"], 
														this["_card" + (i + 1) + "DontUse"]];
						
						break;
					}
				}
				
				//If it is, decide whether or not to use it
				if (_rerollInHand)
				{
					//USE
					if (MakeDecision(_rerollValues) == this["FACTOR_CARD_" + (i + 1) + "_USE"])
					{
						//Click the card so it will be used
						cardSlots[i].dispatchEvent(new MouseEvent(MouseEvent.CLICK));
						
						//Next task: "Read"the opened card dialog.
						_state = STATE_ANALYZING_CARD_DIALOG;
					}
					//DON'T USE
					else
					{
						//Next task: Check if a card can be drawn.
						_state = STATE_CHECKING_DRAW;
					}
				}
				//Otherwise, regroup
				else
				{
					//Next task: Regroup.
					_state = STATE_CHECKING_DRAW;
				}
			}
			else if (_state == STATE_CHECKING_DRAW)
			{
				//Execute task: Check to see if a card can be drawn.
				_statusScreen.gotoAndPlay("thinking");
				
				//Always get the current state of the board before moving on
				SetKnowledges();
				
				if (_main.btnDraw.visible)
				{
					//Next task: Draw card.
					_state = STATE_DRAWING_CARD;
				}
				else
				{
					//Next task: Decide amongst playing card(s) in hand, placing piece, or ending turn.
					_state = STATE_ANALYZING_OPTIONS;
				}
			}
			else if (_state == STATE_DRAWING_CARD)
			{
				//Execute task: Draw card.
				_statusScreen.gotoAndPlay("drawing");
				_main.btnDraw.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				
				//Check for negative card drawn
				if (_main.screenCardDialog)
				{
					//Keep AI console on top, so P1 can't click anything on it
					_statusScreen.parent.addChild(_statusScreen);
					
					//Next task: "Read" the card dialog
					_state = STATE_ANALYZING_CARD_DIALOG;
				}
				//If no negative card, decide other things
				else
				{
					_noPlayableCards = false;	//can potentially play a card now
					
					//Next task: Decide play versus place.
					_state = STATE_ANALYZING_OPTIONS;
				}
			}
			else if (_state == STATE_ANALYZING_OPTIONS)
			{
				//Execute task: Decide play versus place.
				_statusScreen.gotoAndPlay("thinking");
				
				//If PLAYABLE cards in hand AND piece can be placed, decide between the two...
				if (hand.length > 0 && !_noPlayableCards && (_main.firstSquare.highlighter.visible || _main.secondSquare.highlighter.visible))
				{
					var _factorList1:Array = [	FACTOR_PLACE_PIECE_THEN_PLAY_CARD, _placePieceThenPlayCard,
												FACTOR_PLAY_CARD_THEN_PLACE_PIECE, _playCardThenPlacePiece	];
					
					//Place piece then play card chosen
					if (MakeDecision(_factorList1) == FACTOR_PLACE_PIECE_THEN_PLAY_CARD)
					{
						//Next task: Search board for viable squares.
						_state = STATE_ANALYZING_BOARD;
					}
					//Play card then place piece chosen
					else
					{
						//Next task: Choose card to play.
						_state = STATE_ANALYZING_CARDS;
					}
				}
				//...Otherwise, if no PLAYABLE cards in hand AND piece can be placed, place piece.
				else if ((hand.length == 0 || _noPlayableCards) && (_main.firstSquare.highlighter.visible || _main.secondSquare.highlighter.visible))
				{
					//Next task: Search board for viable squares.
					_state = STATE_ANALYZING_BOARD;
				}
				//...Otherwise, if PLAYABLE cards in hand AND no piece can be placed, check hand for playable cards.
				else if (hand.length > 0 && !_noPlayableCards && !_main.firstSquare.highlighter.visible && !_main.secondSquare.highlighter.visible)
				{
					//Next task: Choose card to play.
					_state = STATE_ANALYZING_CARDS;
				}
				//...Otherwise, if no PLAYABLE card in hand AND no piece can be placed, end turn.
				else
				{
					_state = STATE_ENDING_TURN;
				}
			}
			else if (_state == STATE_ANALYZING_CARD_DIALOG)
			{
				//Execute task: Deal with card dialog.
				_statusScreen.gotoAndPlay("thinking");
				var _openedCard:String = _main.screenCardDialog.currentLabel;
				
				//Play the card if possible
				if (_main.screenCardDialog.btnPlayCard.visible)
				{
					_main.screenCardDialog.btnPlayCard.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					
					//ADD PIECE OR REMOVE PIECE
					if 		(_openedCard == "addpiece" || _openedCard == "removepiece")
					{
						//Next task: Analyze board.
						_state = STATE_ANALYZING_BOARD;
					}
					//BATTLE
					else if (_openedCard == "battle")
					{
						//Next task: Start battle.
						_state = STATE_STARTING_BATTLE;
					}
					//SHIELDED
					else if (_main.screenShielded)
					{
						//Next task: "Read" shielded dialog.
						_state = STATE_READING_SHIELDED;
						
						_statusScreen.parent.addChild(_statusScreen);
					}
					else
					{
						//Next task: Regroup.
						_state = STATE_CHECKING_DRAW;
					}
				}
				//Otherwise, close the card dialog and decide what to do next
				else
				{
					_main.screenCardDialog.btnClose.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					
					//Next task: Regroup.
					_state = STATE_CHECKING_DRAW;
				}
			}
			else if (_state == STATE_ANALYZING_CARDS)
			{
				//Execute task: Decide which card in hand to play.
				_statusScreen.gotoAndPlay("thinking");
				_noPlayableCards = true;
				
				//Loop through each card, opening it and checking to see if it can be played
				for (i = 0; i < hand.length; i++)
				{
					//Open this card
					cardSlots[i].dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					
					//If it can be played, break the loop
					if (_main.screenCardDialog.btnPlayCard.visible)
					{
						trace("card dialog play button is visible");
						
						//Close it either way
						_main.screenCardDialog.btnClose.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
						
						_noPlayableCards = false;
						
						break;
					}
					
					//Close it either way
					_main.screenCardDialog.btnClose.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
				
				//If no cards can be played, regroup
				if (_noPlayableCards)
				{
					//Next task: Regroup.
					_state = STATE_CHECKING_DRAW;
				}
				//Otherwise, choose a card to play
				else
				{
					//Next task: Choose viable card.
					_state = STATE_CHOOSING_CARD;
				}
			}
			else if (_state == STATE_ANALYZING_BOARD)
			{
				//Execute task: Fake decide which piece to click.
				_statusScreen.gotoAndPlay("thinking");
				
				//Next task: Click the highest valued square on the board.
				_state = STATE_CLICKING_SQUARE;
			}
			else if (_state == STATE_CHOOSING_CARD)
			{
				//Execute task: Choose card from hand to play.
				_statusScreen.gotoAndPlay("thinking");
				var _selectedCard:MovieClip;
				
				//First, search for any no-brainer cards and play them immediately
				for (i = 0; i < hand.length; i++)
				{
					if ((hand[i] == "stealcard" && opponent.hand.length > 0) || hand[i] == "extraturn")
					{
						_selectedCard = cardSlots[i];
						
						break;
					}
				}
				
				//If a no-brainer card wasn't found, decide which of the other cards to play
				if (!_selectedCard)
				{
					var _factorList:Array = [];
					for (i = 0; i < hand.length; i++)
					{
						if (hand[i] == "addpiece" || hand[i] == "removepiece" || hand[i] == "battle")
						{
							_factorList.push(this["FACTOR_CARD_" + (i + 1) + "_USE"]);
							_factorList.push(this["_card" + (i + 1) + "Use"]);
							_factorList.push(this["FACTOR_CARD_" + (i + 1) + "_DONT_USE"]);
							_factorList.push(this["_card" + (i + 1) + "DontUse"]);
						}
					}
					
					if (_factorList.length > 0) 
					{
						var _decisionResult:String = MakeDecision(_factorList);
						
						//See if any of them were chosen
						if 		(_decisionResult == FACTOR_CARD_1_USE) _selectedCard = cardSlots[0];
						else if	(_decisionResult == FACTOR_CARD_2_USE) _selectedCard = cardSlots[1];
						else if (_decisionResult == FACTOR_CARD_3_USE) _selectedCard = cardSlots[2];
						else if (_decisionResult == FACTOR_CARD_4_USE) _selectedCard = cardSlots[3];
					}
				}
				
				//If one of those cards were chosen, click it
				if (_selectedCard)
				{
					_selectedCard.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					
					_statusScreen.parent.addChild(_statusScreen);	//keep status screen on top so P1 can't click anything
					
					//Next task: Deal with the card dialog.
					_state = STATE_ANALYZING_CARD_DIALOG;
				}
				//Otherwise, regroup
				else
				{
					//Next task: Regroup.
					_state = STATE_CHECKING_DRAW;
				}
			}
			else if (_state == STATE_READING_SHIELDED)
			{
				//Execute task: Close "Shielded" dialog
				_statusScreen.gotoAndPlay("reading");
				
				_main.screenShielded.btnClose.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				
				//Next task: Regroup.
				_state = STATE_CHECKING_DRAW;
			}
			else if (_state == STATE_STARTING_BATTLE)
			{
				//Execute task: Wait for P1 to click their Roll button.
				_statusScreen.gotoAndPlay("thinking");
				
				//If P1 has clicked their battle roll, it's AI's turn to do it
				if (!_main.screenRollBattle.btnRoll1.visible)
				{
					//Next task: Continue battle.
					_state = STATE_BATTLE_ROLLING;
				}
			}
			else if (_state == STATE_BATTLE_ROLLING)
			{
				//Execute task: Click roll battle button.
				_statusScreen.gotoAndPlay("rolling");
				
				_main.screenRollBattle.btnRoll2.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				
				//Next task: Wait for P1 to close the battle dialog.
				_state = STATE_BATTLE_FINISHED;
			}
			else if (_state == STATE_BATTLE_FINISHED)
			{
				//Execute task: Do nothing.
				_statusScreen.gotoAndPlay("thinking");
				
				//WAIT UNTIL P1 TRIGGERS A NEW STATE
			}
			else if (_state == STATE_CLICKING_SQUARE)
			{
				//Execute task: Click the highest valued square.
				_statusScreen.gotoAndPlay("thinking");
				
				//Loop through the board and find the highest-valued piece that is clickable
				var _highestValuedSquare:PatruBoardSquare;
				for (i = 1; i < 7; i++) 
				{
					for (j = 1; j < 7; j ++)
					{
						if (_main["board" + j + i].highlighter.visible)
						{
							if (!_highestValuedSquare || (_highestValuedSquare.aiValue + _highestValuedSquare.aiOpponentValue < _main["board" + j + i].aiValue + _main["board" + j + i].aiOpponentValue))
							{
								_highestValuedSquare = _main["board" + j + i];
							}
						}
					}
				}
				
				//If no highest valued square was found, it means no board squares were clickable, so regroup
				if (!_highestValuedSquare)
				{
					//Next task: Regroup.
					_state = STATE_CHECKING_DRAW;
				}
				//Otherwise, click that square
				else
				{
					_highestValuedSquare.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
					
					//Check for game over
					if (_main.screenGameOver)
					{
						//Next task: END TURN
						_state = STATE_ENDING_TURN;
					}
					else
					{
						//Next task: Regroup
						_state = STATE_CHECKING_DRAW;
					}
				}
			}
			else if (_state == STATE_ENDING_TURN)
			{
				_statusScreen.stop();
				
				_statusScreen.removeEventListener(PatruEvent.AI_ANIMATION_COMPLETE, ExecuteTasks);
				
				_main.btnEndTurn.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				if (_main.screenEndTurnDialog) _main.screenEndTurnDialog.dispatchEvent(new PatruEvent(PatruEvent.END_TURN_CONFIRMED));	//in case end turn dialog appears
				
				return;
			}
		}
		
		/**
		 * Randomly chooses a task from the list of options that was passed, based on all options' weighted values.
		 * 
		 * @param	optionValues	A list of all possible options in this decision.
		 * @return	Returns the string associated with the randomly generated decision.
		 */
		private function MakeDecision(optionValues:Array):String 
		{
			var _optionValuesTotal:int;
			var _randValue:int;
			var _randIndex:int;
			var _randDecision:int;
			
			//1) add the values of all factors (ignoring even indices, which store the strings that correspond to these factors)
			for (var i:int = 1; i < optionValues.length; i+= 2)
			{
				_optionValuesTotal += optionValues[i];
			}
			
			//2) generate random value between 1-total
			_randValue = Math.random() * _optionValuesTotal;
			
			while (true)
			{
				//3) select random factor from list and subtract THAT factors value, also- when selected from list remove from list
				_randIndex = Math.random() * optionValues.length + 1;
				_randDecision = optionValues[_randIndex];
				optionValues.splice(_randIndex, 1);
				_randValue -= _randDecision;
				
				//if result =<0 then return that factor as decision
				if (_randValue <= 0) return optionValues[0];
				
				//if result >0 goto step 3
				else continue;
			}
			
			return null;
		}
		
		/**
		 * Searches in four directions around a specific square, looking for spaces of interest.
		 * 
		 * @param	originSquare	The square that all searching should be based on.
		 * @param	targetPlayer	The player whose perspective this search is based on.
		 * 
		 * @return	Returns the new value for the origin square square, as determined by its evaluated surrounding squares.
		 * 
		 * @see		AnalyzeBoard()
		 */
		private function MapValues(originSquare:PatruBoardSquare, targetPlayer:PatruPlayer):int 
		{
			//Setup Variables
			var XOffSet:int;
			var YOffSet:int;
			var _directionValue:int;
			var _overallValue:int;
			
			//var _originSquareCol:int = int(originSquare.name.substr(5, 1));
			//var _originSquareRow:int = int(originSquare.name.substr(6, 1));
			
			var _currentSquare:PatruBoardSquare;
			
			//Needs to check in two direction at a time.  Up/down, UpRight/DownLeft, Right/Left, DownRight/UpLeft
			///Increment direction and set Offsets
			for (var i:int = 1; i < 9; i++)
			{
				//Up
				if 		(i == 1)
				{
					_directionValue = 1;
					
					XOffSet = 0;
					YOffSet = -1;
				}
				//Down
				else if (i == 2)
				{
					XOffSet = 0;
					YOffSet = 1;
				}
				//UpRight
				else if (i == 3)
				{
					if (_overallValue < _directionValue) _overallValue = _directionValue;
					
					_directionValue = 1;
					
					XOffSet = 1;
					YOffSet = -1;
				}
				//DownLeft
				else if (i == 4)
				{
					XOffSet = -1;
					YOffSet = 1;
				}
				//Right
				else if (i == 5)
				{
					if (_overallValue < _directionValue) _overallValue = _directionValue;
					
					_directionValue = 1;
					
					XOffSet = 1;
					YOffSet = 0;
				}
				//Left
				else if (i == 6)
				{
					XOffSet = -1;
					YOffSet = 0;
				}
				//DownRight
				else if (i == 7)
				{
					if (_overallValue < _directionValue) _overallValue = _directionValue;
					
					_directionValue = 1;
					
					XOffSet = 1;
					YOffSet = 1;
				}
				//UpLeft
				else if (i == 8)
				{
					XOffSet = -1;
					YOffSet = -1;
				}
				else trace("bad index");
				
				//Increment position checked
				for (var j:int = 1; j < 4; j++)
				{
					_currentSquare = null;
					
					try
					{
						_currentSquare = _main["board" + (originSquare.col + XOffSet * j) + (originSquare.row + YOffSet * j)];
					}
					catch (err:Error)
					{
						//trace("can't do row/col: " + (originSquare.col + XOffSet * j), (originSquare.row + YOffSet * j));
					}
					
					if (_currentSquare && _currentSquare.currentFrame == targetPlayer.pieceFrame)
					{
						_directionValue++;
					}
					else if (_currentSquare && _currentSquare.currentFrame == targetPlayer.opponent.pieceFrame)
					{
						break;
					}
				}
			}
			
			if (_overallValue < _directionValue) _overallValue = _directionValue;
			
			return _overallValue;
		}
		
		/**
		 * Modify properties that will add weight to specific decisions that may have to be made.
		 */
		private function SetInfluences():void 
		{
			var i:int;
			var j:int;
			var k:int;
			var l:int;
			
			//Order of play Factors: (20value by default)
			
			/**
			 * 1. PlayCardThenPlacePiece
			 */
			for (i = 0; i < hand.length; i++)
			{
				//If Cards in hand contain Reroll +½ Influence
				if (cardSlots[i].currentLabel == "reroll")
				{
					_playCardThenPlacePiece += INFLUENCE / 2;
					
					//If Cards in hand contain Reroll && Roll1 = Roll2 && BoardCount>60% +Influence
					if (_main.btnDraw.visible && (numPieces + opponent.numPieces > 22)) _placePieceThenPlayCard += INFLUENCE;
					
					break;
				}
			}
			
			for (i = 0; i < hand.length; i++)
			{
				//If Cards in hand contain Remove Opponent’s piece && ...
				if (cardSlots[i].currentLabel == "removepiece")
				{
					//BoardLocation has 4value for AI && ... 
					for (j = 0; j < _aiBoardValues.length; j++)
					{
						for (k = 0; k < _aiBoardValues[j].length; k++)
						{
							if (_aiBoardValues[j][k] >= 4)
							{
								//contains players chip +2Influence
								if (_main["board" + (j + 1) + (k + 1)].currentFrame == opponent.pieceFrame)
								{
									_playCardThenPlacePiece += INFLUENCE * 2;
								}
							}
						}
					}
					
					break;
				}
			}
			
			
			/**
			 * 2. PlacePieceThenPlayCard
			 */
			//If BoardChoice1 && BoardChoice2 are open +2Influence
			if (_main.firstSquare.currentLabel == "blank" && _main.secondSquare.currentLabel == "blank")
			{
				_placePieceThenPlayCard += INFLUENCE * 2;
			}
			
			//If BoardChoice1 || BoardChoice2 are 4value for AI +2 Influence
			if (_aiBoardValues[_main.roll1 - 1][_main.roll2 - 1] >= 4 || _aiBoardValues[_main.roll2 - 1][_main.roll1 - 1] >= 4)
			{
				_placePieceThenPlayCard += INFLUENCE * 2;
			}
			
			
			//Card Factors: (0value by default)
			
			var _currentCardUse:int;
			var _currentCardDontUse:int;
			for (i = 0; i < hand.length; i++)
			{
				_currentCardUse = this["_card" + (i + 1) + "Use"];
				_currentCardDontUse = this["_card" + (i + 1) + "DontUse"];
				
				if (cardSlots[i].currentLabel == "reroll")
				{
					/**
					 * 1. Reroll Use
					 */
						
					//if both values for both squares are <2 +2Influence
					if (_aiBoardValues[_main.roll1 - 1][_main.roll2 - 1] < 2 && _aiBoardValues[_main.roll2 - 1][_main.roll1 - 1] < 2 &&
						_aioBoardValues[_main.roll1 - 1][_main.roll2 - 1] < 2 && _aioBoardValues[_main.roll2 - 1][_main.roll1 - 1] < 2)
					{
						_currentCardUse += INFLUENCE * 2;
					}
					
					//if there is only 1 open board choice and that 1 open board choice's ai value is < 4 +Influence
					if (_main.firstSquare.currentLabel == "blank" && _main.secondSquare.currentLabel != "blank")
					{
						if (_aiBoardValues[_main.roll1 - 1][_main.roll2 - 1] < 4)
						{
							_currentCardUse += INFLUENCE;
						}
					}
					else if (_main.firstSquare.currentLabel != "blank" && _main.secondSquare.currentLabel == "blank")
					{
						if (_aiBoardValues[_main.roll2 - 1][_main.roll1 - 1] < 4)
						{
							_currentCardUse += INFLUENCE;
						}
					}
					
					//if there is only 1 open board choice and that 1 open board choice's ai OPPONENT value is < 4 +Influence
					if (_main.firstSquare.currentLabel == "blank" && _main.secondSquare.currentLabel != "blank")
					{
						if (_aioBoardValues[_main.roll1 - 1][_main.roll2 - 1] < 4)
						{
							_currentCardUse += INFLUENCE;
						}
					}
					else if (_main.firstSquare.currentLabel != "blank" && _main.secondSquare.currentLabel == "blank")
					{
						if (_aioBoardValues[_main.roll2 - 1][_main.roll1 - 1] < 4)
						{
							_currentCardUse += INFLUENCE;
						}
					}
					
					//if Roll1=Roll2 && BoardCount>60% +2Influence
					if (_main.btnDraw.visible && numPieces + opponent.numPieces > 22) _currentCardUse += INFLUENCE;
					
					/**
					 * 1. Reroll Don't Use
					 */
					
					//if AI value & Player value for BoardChoice1 & BoardChoice2 are > 2 +3Influence
					if (_aiBoardValues[_main.roll1 - 1][_main.roll2 - 1] > 2 &&
						_aiBoardValues[_main.roll2 - 1][_main.roll1 - 1] > 2 &&
						_aioBoardValues[_main.roll1 - 1][_main.roll2 - 1] > 2 &&
						_aioBoardValues[_main.roll2 - 1][_main.roll1 - 1] > 2)
					{
						_currentCardDontUse += INFLUENCE * 3;
					}
					
					//if BoardCount <40% +2Influence
					if (numPieces + opponent.numPieces < 14) _currentCardDontUse += INFLUENCE * 2;
				}
				else if (cardSlots[i].currentLabel == "removepiece")
				{
					/**
					 * 2. Remove Piece Use
					 */
					
					//If BoardChoice has 4value for AI && player has a piece there +Influence
					removePieceUseLoop1: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aiBoardValues[k - 1][j - 1] >= 4 && _main["board" + k + j].currentFrame == opponent.pieceFrame)
							{
								_currentCardUse += INFLUENCE;
								
								break removePieceUseLoop1;
							}
						}
					}
					
					//If BoardLocation has 4value for Player +Influence
					removePieceUseLoop2: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aioBoardValues[k - 1][j - 1] >= 4)
							{
								_currentCardUse += INFLUENCE;
								
								break removePieceUseLoop2;
							}
						}
					}
					
					/**
					 * 2. Remove Piece Don't Use
					 */
					
					//1~If BoardCount <25% +2Influence
					if (numPieces + opponent.numPieces < 9) _currentCardDontUse += INFLUENCE * 2;
					
					//1~If BoardCount <45% +Influence
					else if (numPieces + opponent.numPieces < 16) _currentCardDontUse += INFLUENCE;
					
					/*Target:
					
					Primary – Available BoardChoice with 4value for AI that is blocked by player
					
					Secondary –Random BoardLocation with 4value for AI && highest neighbor AI total value that contains player chip
					
					Tertiary - Random BoardLocation with 4value for player && highest neighbor AI total value that contains player chip*/
				}
				else if (cardSlots[i].currentLabel == "addpiece")
				{
					/**
					 * 3. Add Piece Don't Use
					 */
					
					//If BoardLocation has 4value for AI +2Influence
					addPieceUseLoop1: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aiBoardValues[k - 1][j - 1] >= 4)
							{
								_currentCardUse += INFLUENCE * 2;
								
								break addPieceUseLoop1;
							}
						}
					}
					
					//If BoardLocation has 4value for Player +Influence
					addPieceUseLoop2: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aioBoardValues[k - 1][j - 1] >= 4)
							{
								_currentCardUse += INFLUENCE;
								
								break addPieceUseLoop2;
							}
						}
					}
					
					/**
					 * 3. Add Piece Don't Use
					 */
					
					//Base 20 value
					_currentCardDontUse += INFLUENCE;
					
					//If BoardLocation has 4value for Player && BoardCount <40% +1/2Influence
					if (numPieces + opponent.numPieces < 14)
					{
						addPieceDontUseLoop1: for (j = 1; j < 7; j++)
						{
							for (k = 1; k < 7; k++)
							{
								if (_aioBoardValues[k - 1][j - 1] >= 4)
								{
									_currentCardUse += INFLUENCE / 2;
									
									break addPieceDontUseLoop1;
								}
							}
						}
					}
					
					/*Target:
					
					Primary – BoardLocation with 4value for AI
					
					Secondary – BoardLocation with 4value for Player */
				}
				else if (cardSlots[i].currentLabel == "battle")
				{
					/**
					 * 4. Battle Use
					 */
					
					//If BoardLocation has 4value for AI +3Influence
					battleUseLoop1: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aiBoardValues[k - 1][j - 1] >= 4)
							{
								_currentCardUse += INFLUENCE * 3;
								
								break battleUseLoop1;
							}
						}
					}
					
					//If BoardLocation has 4value for Player && BoardCount >60% +Influence
					if (numPieces + opponent.numPieces > 21)
					{
						battleUseLoop2: for (j = 1; j < 7; j++)
						{
							for (k = 1; k < 7; k++)
							{
								if (_aioBoardValues[k - 1][j - 1] >= 4)
								{
									_currentCardUse += INFLUENCE;
									
									break battleUseLoop2;
								}
							}
						}
					}
					
					/**
					 * 4. Battle Don't Use
					 */
					
					//If BoardCount<40% +Influence
					if (numPieces + opponent.numPieces < 14) _currentCardDontUse += INFLUENCE;
					
					//If no BoardLocation has 4value for player +Influence
					var _noAIO4Found:Boolean = true;
					battleDontUseLoop1: for (j = 1; j < 7; j++)
					{
						for (k = 1; k < 7; k++)
						{
							if (_aioBoardValues[k - 1][j - 1] >= 4)
							{
								_noAIO4Found = false;
								
								break battleDontUseLoop1;
							}
						}
					}
					if (_noAIO4Found) _currentCardDontUse += INFLUENCE;
					
					/*Target:
					
					Primary – BoardLocation with 4value for AI
					
					Secondary – BoardLocation with 4value for Player && Highest neighbor total value*/
				}
			}
		}
		
		/**
		 * Attain as much information as possible about the state of the game in order to make better decisions about what to do next.
		 */
		private function SetKnowledges():void 
		{
			AnalyzeBoard();
			
			_playCardThenPlacePiece = 20;
			_placePieceThenPlayCard = 20;
			
			/*_rerollUse = 0;
			_rerollDontUse = 0;
			_removePieceUse = 0;
			_removePieceDontUse = 0;
			_addPieceUse = 0;
			_addPieceDontUse = 0;
			_battleUse = 0;
			_battleDontUse = 0;*/
			
			_card1Use = 0;
			_card2Use = 0;
			_card3Use = 0;
			_card4Use = 0;
			_card1DontUse = 0;
			_card2DontUse = 0;
			_card3DontUse = 0;
			_card4DontUse = 0;
			
			SetInfluences();
		}
		
		
		//GETTER/SETTERS
		public function set statusScreen(value:PatruScreenAIStatus):void 
		{
			_statusScreen = value;
		}
	}
}