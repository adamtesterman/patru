package 
{
	import flash.display.MovieClip;
	
	import utils.SoundEngine;
	
	import caurina.transitions.Tweener;
	
	/**
	 * ...
	 * @author Adam Testerman
	 */
	public class PatruBoardSquare extends MovieClip
	{
		//PROPERTIES / Constants (Audio Labels)
		private const SFX_ADD_PIECE_01:String =			"AddPiece01";
		private const SFX_REMOVE_PIECE_01:String =		"RemovePiece01";
		
		//PROPERTIES / Variables
		private var _col:int;
		private var _row:int;
		private var _aiValue:int;
		private var _aiOpponentValue:int;
		private var _filled:Boolean;					//whether or not there is a piece in this square
		
		//PROPERTIES / Objects
		private var _se:SoundEngine = SoundEngine.getInstance();
		private var _piece:MovieClip;
		
		
		public function PatruBoardSquare():void 
		{
			//New properties
			_col = int(name.substr(5, 1));
			_row = int(name.substr(6, 1));
			
			//Inherited methods
			stop();
		}
		
		
		/**
		 * Causes a piece icon to tween into the board square, providing a little more polish when a piece is added.
		 */
		public function tweenPieceIn():void 
		{
			addFrameScript(currentFrame - 1, FinishTweenPieceIn);	//required because playhead needs to fully hit the frame before the piece can be referenced and tweened
			
			_se.playSound(SFX_ADD_PIECE_01);
		}
		
		/**
		 * Causes a piece icon to tween out of the board square, providing a little more polish when a piece is removed.
		 */
		public function tweenPieceOut():void 
		{
			Tweener.addTween(_piece, { scaleX:0, scaleY:0, time:.75, transition:"easeOutBack", onComplete:FinishTweenPieceOut } );
			
			_se.playSound(SFX_REMOVE_PIECE_01);
		}
		
		/**
		 * Applies the tween to the piece icon. Required because playhead needs to fully hit the frame before the piece can be referenced and tweened.
		 */
		private function FinishTweenPieceIn():void 
		{
			//Find the piece icon movie clip inside the square (they don't have instance names)
			_piece = MovieClip(getChildAt(2));
			
			_piece.scaleX = _piece.scaleY = 0;
			Tweener.addTween(_piece, { scaleX:1, scaleY:1, time:.75, transition:"easeOutElastic" } );
		}
		
		/**
		 * Causes this square to go to the "blank" 
		 */
		private function FinishTweenPieceOut():void 
		{
			if (!_filled) gotoAndStop("blank");
		}
		
		
		//GETTER/SETTERS
		public function get aiOpponentValue():int 
		{
			return _aiOpponentValue;
		}
		
		public function set aiOpponentValue(value:int):void 
		{
			_aiOpponentValue = value;
			
			//txtAIO.text = String(_aiOpponentValue);
		}
		
		public function get aiValue():int 
		{
			return _aiValue;
		}
		
		public function set aiValue(value:int):void 
		{
			_aiValue = value;
			
			//txtAIV.text = String(_aiValue);			
		}
		
		public function get filled():Boolean 
		{
			return _filled;
		}
		
		public function set filled(value:Boolean):void 
		{
			_filled = value;
		}		
		
		public function get col():int 
		{
			return _col;
		}
		
		public function get row():int 
		{
			return _row;
		}
	}	
}